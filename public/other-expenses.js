var storeName
var store = new URL(window.location.href).searchParams.get('store')
var role = new URL(window.location.href).searchParams.get('role')
var eventID = new URL(window.location.href).searchParams.get('eventID')
var d = new Date()
var keyEdit

$(document).ready(() => {
  $(`#formOther`).css(`height`, $(window).height() - 104)
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(window).height() - 56)
    $('.row-menu').css('mex-height', $(window).height() - 56)
  }
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
    storeName = snapshot.val()
  })

  var d = new Date()
  $(`#moreNow`).html(`ประจำเดือน ${monthNames[d.getMonth()]} ปี ${thaiYear(d.getFullYear())}`)

  if (eventID != null) {
    firebase.database().ref(`stores/${store}/cost/other/${d.getFullYear()}-${d.getMonth()}/${eventID}`).on(`value`, snapshot => {
      $(`#costDetail`).val(snapshot.val()[`detail`].replace(/<br>/g, '\n'))
      $(`#moreValue`).val(snapshot.val()[`value`])
      $(`#save-more`).attr(`disabled`, false)
    })
  }

  getHistory()
})

$('#delConfirm').keyup(() => {
  if ($('#delConfirm').val() !== storeName) {
    $('#delConfirm-btn').prop('disabled', true)
  } else {
    $('#delConfirm-btn').prop('disabled', false)
  }
})

$('#delConfirm-btn').click(() => {
  firebase.database().ref('stores/' + store).remove().then(() => {
    window.location.href = './store-list.html'
  }).catch((error) => {
    console.log(error)
  })
})

window.onresize = (event) => {
  $('.row-menu').css('height', $(window).height() - 56)
}

const offlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                        <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                      </a>
                    </li>
                    <li class="other">
                      <a role="button" href="#" class="text-muted">
                        <i class="material-icons">delete</i> ลบร้านค้า
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        localforage.getItem('storesManager', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'staffer') {
        window.location.href = `./update-sales.html?store=${store}&role=staffer`
      }
    } else {
      window.location.href = './index.html'
    }
  })
}

const onlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                          <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                            <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./update-sales.html?store=${store}&role=owner">
                            <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-request.html?store=${store}&role=owner">
                            <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-list.html?store=${store}&role=owner">
                            <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                          </a>
                        </li>
                        <li class="menu_active">
                          <a rel="noopener" href="./expenses.html?store=${store}&role=owner">
                            <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./edit-access.html?store=${store}&role=owner">
                            <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./create-menulist.html?store=${store}&role=owner">
                            <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./edit-store.html?store=${store}&role=owner">
                            <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                          </a>
                        </li>
                        <li class="del_store">
                          <a role="button" href="#" data-toggle="modal" data-target="#delStore">
                            <i class="material-icons">delete</i> ลบร้านค้า
                          </a>
                        </li>
                        <li class="back_page">
                          <a role="button" href="./store-list.html">
                            <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                          </a>
                        </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
          $(`.electricity`).attr(`onclick`, `window.location.href='./electricity.html?store=${store}&role=owner'`)
          $(`.water`).attr(`onclick`, `window.location.href='./water.html?store=${store}&role=owner'`)
          $(`.salary`).attr(`onclick`, `window.location.href='./salary.html?store=${store}&role=owner'`)
          $(`.rent`).attr(`onclick`, `window.location.href='./rent.html?store=${store}&role=owner'`)
          $(`.other-expenses`).attr(`onclick`, `window.location.href='./other-expenses.html?store=${store}&role=owner'`)
        })
      } else if (role === 'manager') {
        localforage.getItem('storesManager', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=manager">
                          <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-request.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=manager">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-assessment.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="menu_active">
                        <a rel="noopener" href="./expenses.html?store=${store}&role=manager">
                          <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
          $(`.electricity`).attr(`onclick`, `window.location.href='./electricity.html?store=${store}&role=manager'`)
          $(`.water`).attr(`onclick`, `window.location.href='./water.html?store=${store}&role=manager'`)
          $(`.salary`).attr(`onclick`, `window.location.href='./salary.html?store=${store}&role=manager'`)
          $(`.rent`).attr(`onclick`, `window.location.href='./rent.html?store=${store}&role=manager'`)
          $(`.other-expenses`).attr(`onclick`, `window.location.href='./other-expenses.html?store=${store}&role=manager'`)
        })
      } else if (role === 'staffer') {
        window.location.href = `./update-sales.html?store=${store}&role=staffer`
      }
      firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
        $('#delStoreName').text(snapshot.val())
      })
    } else {
      window.location.href = './index.html'
    }
  })
}

window.addEventListener('online', () => {
  onlineWorker()
})

window.addEventListener('offline', () => {
  offlineWorker()
})

const inputNumber = (e) => {
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
  (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode >= 35 && e.keyCode <= 39)) {
      return;
  }
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
}

const formatMoney = (num) => {
  if (num !== ``) {
    var p = num.toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
  } else {
    return `0.00`
  }
}

$('#moreValue').keyup(function() {
  if ($(this).val() != null && $(this).val() != `` && $(`#costDetail`).val() != null && $(`#costDetail`).val() != ``) {
    $(`#save-more`).prop(`disabled`, false)
  } else {
    $(`#save-more`).prop(`disabled`, true)
  }
})

$('#costDetail').keyup(function() {
  if ($(this).val() != null && $(this).val() != `` && $(`#moreValue`).val() != null && $(`#moreValue`).val() != ``) {
    $(`#save-more`).prop(`disabled`, false)
  } else {
    $(`#save-more`).prop(`disabled`, true)
  }
})

$(`#save-more`).click(()=>{
  if (eventID != null) {
    $(`#savedModal`).modal()
    $(`#historyList`).empty()
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        var user = firebase.auth().currentUser
        firebase.database().ref(`stores/${store}/cost/other/${d.getFullYear()}-${d.getMonth()}`).child(eventID).set({
          date : `${d.getDate()}-${d.getMonth()}-${d.getFullYear()}`,
          user : user.uid,
          value : $(`#moreValue`).val(),
          detail : $(`#costDetail`).val().replace(/\r?\n/g, '<br>')
        })
      }
    })
    getHistory()
  } else {
    $(`#savedModal`).modal()
    $(`#historyList`).empty()
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        var user = firebase.auth().currentUser
        firebase.database().ref(`stores/${store}/cost/other/${d.getFullYear()}-${d.getMonth()}`).push({
          date : `${d.getDate()}-${d.getMonth()}-${d.getFullYear()}`,
          user : user.uid,
          value : $(`#moreValue`).val(),
          detail : $(`#costDetail`).val().replace(/\r?\n/g, '<br>')
        })
      }
    })
    getHistory()
  }
})

function getHistory() {
  firebase.database().ref(`stores/${store}/cost/other/${d.getFullYear()}-${d.getMonth()}`).on(`value`, snapshot => {
    $(`#historyList`).empty()
    if (snapshot.val() != null) {
      for (var key in snapshot.val()) {
        var txt = `<div class="history-list py-1">
                    <div class="row m-0">
                      <div class="col-8">
                        <p class="m-0">${snapshot.val()[key][`detail`]}</p>
                        <p class="m-0 text-danger">${formatMoney(parseFloat(snapshot.val()[key][`value`]))} บาท</p>
                        <p class="m-0 text-muted" id="text-${key}"></p>
                      </div>
                      <div class="col-4 text-right">
                        <p class="mt-1" onclick="editOther('${key}')"><i class="material-icons text-edit">edit</i></p>
                        <p class="m-0" onclick="delOther('${key}')"><i class="material-icons text-danger">delete</i></p>
                      </div>
                    </div>
                  </div>`
        $(`#historyList`).append(txt)
        firebase.database().ref(`users/${snapshot.val()[key][`user`]}`).on(`value`, snapshot => {
          $(`#text-${key}`).html(`บันทึกโดย ${snapshot.val()[`fullname`]}`)
        })
      }
    } else {
      var txt = `<div class="text-center py-5">
                  <p class="text-muted my-5">ยังไม่มีการบันทึกรายจ่ายอื่น ๆ</p>
                </div>`
        $(`#historyList`).append(txt)
    }
  })
}

function editOther(key) {
  window.location.href = `./other-expenses.html?store=${store}&role=${role}&eventID=${key}`
}

function delOther(key) {
  $(`#historyList`).empty()
  firebase.database().ref(`stores/${store}/cost/other/${d.getFullYear()}-${d.getMonth()}/${key}`).remove()
  getHistory()
}

$("#costDetail").keyup(function(event) {
  if (event.keyCode === 13) {
    $("#save-more").click();
  }
})

$("#moreValue").keyup(function(event) {
  if (event.keyCode === 13) {
    $("#save-more").click();
  }
})

$('form').on('keyup keypress', function (e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) {
    e.preventDefault();
    return false;
  }
})