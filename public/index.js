$(document).ready(() => {
  if (navigator.onLine) {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        var userRef = firebase.database().ref("users/" + user.uid)
        userRef.on('value', snapshot => {
          var userData = '{"' + snapshot.key + '":' + JSON.stringify(snapshot.val()) + '}'
          localforage.setItem('users', JSON.parse(userData))
        })
        var storeRef = firebase.database().ref('stores')
        storeRef.orderByChild('userList/' + user.uid).equalTo('owner').on('value', (snapshot) => {
          localforage.setItem('storesOwner', snapshot.val()).then(() => {
            window.location.href = './store-list.html'
          })
        })
        storeRef.orderByChild('userList/' + user.uid).equalTo('manager').on('value', (snapshot) => {
          localforage.setItem('storesManager', snapshot.val()).then(() => {
            window.location.href = './store-list.html'
          })
        })
        storeRef.orderByChild('userList/' + user.uid).equalTo('staffer').on('value', (snapshot) => {
          localforage.setItem('storesStaffer', snapshot.val()).then(() => {
            window.location.href = './store-list.html'
          })
        })
      } else {
        window.location.href = './sign-in.html'
      }
    })
  } 
  else {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        window.location.href = './store-list.html'
      } else {
        window.location.href = './sign-in.html'
      }
    })
  }
})