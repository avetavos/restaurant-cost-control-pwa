var storeName
var store = new URL(window.location.href).searchParams.get('store')

$(document).ready(() => {
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(window).height() - 56)
    $('.row-menu').css('mex-height', $(window).height() - 56)
  }
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
    storeName = snapshot.val()
  })

  firebase.database().ref('stores/' + store).on('value', snapshot => {
    $(`#storename`).val(snapshot.val()[`storeName`])
    $(`#storetype`).val(snapshot.val()[`storeType`])
    $(`#storeaddress`).val(snapshot.val()[`storeAdd`].replace(/<br>/g, '\n'))
  })
})

$('#delConfirm').keyup(() => {
  if ($('#delConfirm').val() !== storeName) {
    $('#delConfirm-btn').prop('disabled', true)
  } else {
    $('#delConfirm-btn').prop('disabled', false)
  }
})

$('#delConfirm-btn').click(() => {
  firebase.database().ref('stores/' + store).remove().then(() => {
    window.location.href = './store-list.html'
  }).catch((error) => {
    console.log(error)
  })
})

const offlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                        <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                      </a>
                    </li>
                    <li class="other">
                      <a role="button" href="#" class="text-muted">
                        <i class="material-icons">delete</i> ลบร้านค้า
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        window.location.href = './update-sales.html?store=' + store + '&role=manager'
      } else if (role === 'staffer') {
        window.location.href = './update-sales.html?store=' + store + '&role=staffer'
      }
    } else {
      window.location.href = './index.html'
    }
  })
}

const onlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                          <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                            <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./update-sales.html?store=${store}&role=owner">
                            <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-request.html?store=${store}&role=owner">
                            <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-list.html?store=${store}&role=owner">
                            <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./expenses.html?store=${store}&role=owner">
                            <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./edit-access.html?store=${store}&role=owner">
                            <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./create-menulist.html?store=${store}&role=owner">
                            <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                          </a>
                        </li>
                        <li class="menu_active">
                          <a rel="noopener" href="./edit-store.html?store=${store}&role=owner">
                            <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                          </a>
                        </li>
                        <li class="del_store">
                          <a role="button" href="#" data-toggle="modal" data-target="#delStore">
                            <i class="material-icons">delete</i> ลบร้านค้า
                          </a>
                        </li>
                        <li class="back_page">
                          <a role="button" href="./store-list.html">
                            <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                          </a>
                        </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        window.location.href = './update-sales.html?store=' + store + '&role=manager'
      } else if (role === 'staffer') {
        window.location.href = './update-sales.html?store=' + store + '&role=staffer'
      }
      firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
        $('#delStoreName').text(snapshot.val())
      })
    } else {
      window.location.href = './index.html'
    }
  })
}

window.addEventListener('online', () => {
  onlineWorker()
})

window.addEventListener('offline', () => {
  offlineWorker()
})

window.onresize = (event) => {
  $('.row-menu').css('height', $(window).height() - 56)
}

$('#save-edit-store').click(() => {
  firebase.database().ref(`stores/${store}`).update({
    storeAdd: $('#storeaddress').val().replace(/\r?\n/g, '<br>'),
    storeName: $('#storename').val(),
    storeType: $('#storetype').val()
  }).then(() => {
    var user = firebase.auth().currentUser
    firebase.database().ref('stores').orderByChild(`userList/${user.uid}`).equalTo('owner').on('value', snapshot => {
      localforage.setItem('storesOwner', snapshot.val())
    })
    $('#show_name').text($('#storename').val())
    $('#show_name_md').text($('#storename').val())
  })
  $('#save-modal').modal()
})