var d = new Date()
var month = monthNameAgo()
var corData = [0, 0, 0, 0]
var colData = [0, 0, 0, 0]
var cosData = [0, 0, 0, 0]
var otherData = [0, 0, 0, 0]
var electData = [0, 0, 0, 0]
var waterData = [0, 0, 0, 0]
var profitData = [0, 0, 0, 0]
var allCost = [0, 0, 0, 0]
var now = `${d.getFullYear()}-${d.getMonth()}`
var oneMonthAgo = monthAgo(1)
var twoMonthAgo = monthAgo(2)
var threeMonthAgo = monthAgo(3)

function monthAgo(month) {
  if ((d.getMonth() - month) <= 0) {
    if ((d.getMonth() - month) == -1) {
      return (`${d.getFullYear() - 1}-11`)
    } else if ((d.getMonth() - month) == -2) {
      return (`${d.getFullYear() - 1}-10`)
    } else if ((d.getMonth() - month) == -3) {
      return (`${d.getFullYear() - 1}-9`)
    }
  } else {
    return (`${d.getFullYear()}-${d.getMonth() - month}`)
  }
}

function monthNameAgo() {
  var monthTemp = []
  monthTemp[3] = monthNames[d.getMonth()]
  if ((d.getMonth() - 1) == -1) {
    monthTemp[2] = monthNames[11]
    monthTemp[1] = monthNames[10]
    monthTemp[0] = monthNames[9]
  } else if ((d.getMonth() - 1) == -2) {
    monthTemp[2] = monthNames[10]
    monthTemp[1] = monthNames[9]
    monthTemp[0] = monthNames[8]
  } else if ((d.getMonth() - 1) == -3) {
    monthTemp[2] = monthNames[9]
    monthTemp[1] = monthNames[8]
    monthTemp[0] = monthNames[7]
  } else {
    monthTemp[2] = monthNames[d.getMonth() - 1]
    monthTemp[1] = monthNames[d.getMonth() - 2]
    monthTemp[0] = monthNames[d.getMonth() - 3]
  }
  return monthTemp
}

firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    getSalary()
    getSold()
    getRent()
    getOther()
    getProfit()
    getElectricity()
    getWater()
  }
})

function getElectricity() {
  firebase.database().ref(`stores/${store}/cost/electricity/${oneMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      electData[0] += parseInt(snapshot.val()[`value`])
    } else {
      electData[0] += 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/electricity/${twoMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      electData[1] += parseInt(snapshot.val()[`value`])
    } else {
      electData[1] += 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/electricity/${threeMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      electData[2] += parseInt(snapshot.val()[`value`])
    } else {
      electData[2] += 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/electricity/${now}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      electData[3] += parseInt(snapshot.val()[`value`])
    } else {
      electData[3] += 0
    }
    genCos()
  })
}

function getWater() {
  firebase.database().ref(`stores/${store}/cost/water/${oneMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      waterData[0] += parseInt(snapshot.val()[`value`])
    } else {
      waterData[0] += 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/water/${twoMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      waterData[1] += parseInt(snapshot.val()[`value`])
    } else {
      waterData[1] += 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/water/${threeMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      waterData[2] += parseInt(snapshot.val()[`value`])
    } else {
      waterData[2] += 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/water/${now}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      waterData[3] += parseInt(snapshot.val()[`value`])
    } else {
      waterData[3] += 0
    }
    genCos()
  })
}

function getProfit() {
  firebase.database().ref(`stores/${store}/sell/${oneMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var temp = 0
      for (var key in snapshot.val()) {
        temp += (parseInt(snapshot.val()[key][`sales`]) * parseInt(snapshot.val()[key][`price`]))
      }
      profitData[0] = temp
    } else {
      profitData[0] = 0
    }
    allCost[0] = (parseInt(colData[0]) + parseInt(corData[0]) + parseInt(cosData[0]))
    genProfit()
  })
  firebase.database().ref(`stores/${store}/sell/${twoMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var temp = 0
      for (var key in snapshot.val()) {
        temp += (parseInt(snapshot.val()[key][`sales`]) * parseInt(snapshot.val()[key][`price`]))
      }
      profitData[1] = temp
    } else {
      profitData[1] = 0
    }
    allCost[1] = (parseInt(colData[1]) + parseInt(corData[1]) + parseInt(cosData[1]))
    genProfit()
  })
  firebase.database().ref(`stores/${store}/sell/${threeMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var temp = 0
      for (var key in snapshot.val()) {
        temp += (parseInt(snapshot.val()[key][`sales`]) * parseInt(snapshot.val()[key][`price`]))
      }
      profitData[2] = temp
    } else {
      profitData[2] = 0
    }
    allCost[2] = (parseInt(colData[2]) + parseInt(corData[2]) + parseInt(cosData[2]))
    genProfit()
  })
  firebase.database().ref(`stores/${store}/sell/${now}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var temp = 0
      for (var key in snapshot.val()) {
        temp += (parseInt(snapshot.val()[key][`sales`]) * parseInt(snapshot.val()[key][`price`]))
      }
      profitData[3] = temp
    } else {
      profitData[3] = 0
    }
    allCost[3] = (parseInt(colData[3]) + parseInt(corData[3]) + parseInt(cosData[3]))
    genProfit()
    genSuggestion()
  })
}

function getOther() {
  firebase.database().ref(`stores/${store}/cost/other/${oneMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      for (var key in snapshot.val()) {
        otherData[0] += parseInt(snapshot.val()[key][`value`])
      }
    } else {
      otherData[0] += 0
    }
    genCor()
  })
  firebase.database().ref(`stores/${store}/cost/other/${twoMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      for (var key in snapshot.val()) {
        otherData[1] += parseInt(snapshot.val()[key][`value`])
      }
    } else {
      otherData[1] += 0
    }
    genCor()
  })
  firebase.database().ref(`stores/${store}/cost/other/${threeMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      for (var key in snapshot.val()) {
        otherData[2] += parseInt(snapshot.val()[key][`value`])
      }
    } else {
      otherData[2] += 0
    }
    genCor()
  })
  firebase.database().ref(`stores/${store}/cost/other/${now}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      for (var key in snapshot.val()) {
        otherData[3] += parseInt(snapshot.val()[key][`value`])
      }
    } else {
      otherData[3] += 0
    }
    genCor()
  })
}

function getRent() {
  firebase.database().ref(`stores/${store}/cost/rent/${oneMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      corData[0] += parseInt(snapshot.val()[`value`])
    } else {
      corData[0] += 0
    }
    genCor()
  })
  firebase.database().ref(`stores/${store}/cost/rent/${twoMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      corData[1] += parseInt(snapshot.val()[`value`])
    } else {
      corData[1] += 0
    }
    genCor()
  })
  firebase.database().ref(`stores/${store}/cost/rent/${threeMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      corData[2] += parseInt(snapshot.val()[`value`])
    } else {
      corData[2] += 0
    }
    genCor()
  })
  firebase.database().ref(`stores/${store}/cost/rent/${now}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      corData[3] += parseInt(snapshot.val()[`value`])
    } else {
      corData[3] += 0
    }
    genCor()
  })
}

function getSold() {
  firebase.database().ref(`stores/${store}/cost/sold/${oneMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var temp = 0
      for (var key in snapshot.val()) {
        temp += snapshot.val()[key][`value`]
      }
      cosData[0] = temp
    } else {
      cosData[0] = 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/sold/${twoMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var temp = 0
      for (var key in snapshot.val()) {
        temp += snapshot.val()[key][`value`]
      }
      cosData[1] = temp
    } else {
      cosData[1] = 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/sold/${threeMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var temp = 0
      for (var key in snapshot.val()) {
        temp += snapshot.val()[key][`value`]
      }
      cosData[2] = temp
    } else {
      cosData[2] = 0
    }
    genCos()
  })
  firebase.database().ref(`stores/${store}/cost/sold/${now}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var temp = 0
      for (var key in snapshot.val()) {
        temp += snapshot.val()[key][`value`]
      }
      cosData[3] = temp
    } else {
      cosData[3] = 0
    }
    genCos()
  })
}

function getSalary() {
  firebase.database().ref(`stores/${store}/cost/salary/${oneMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      colData[0] = snapshot.val()[`value`]
    } else {
      colData[0] = 0
    }
    genCol()
  })
  firebase.database().ref(`stores/${store}/cost/salary/${twoMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      colData[1] = snapshot.val()[`value`]
    } else {
      colData[1] = 0
    }
    genCol()
  })
  firebase.database().ref(`stores/${store}/cost/salary/${threeMonthAgo}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      colData[2] = snapshot.val()[`value`]
    } else {
      colData[2] = 0
    }
    genCol()
  })
  firebase.database().ref(`stores/${store}/cost/salary/${now}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      colData[3] = snapshot.val()[`value`]
    } else {
      colData[3] = 0
    }
    genCol()
  })
}

function genCos() {
  $(`#cosChart`).empty()
  var cos = document.getElementById("cosChart").getContext('2d')
  var cosChart = new Chart(cos, {
    "type": "bar",
    "data": {
      labels: month,
      datasets: [{
        label: 'ค่าวัตถุดิบ',
        backgroundColor: "#FF3D67",
        data: cosData
      }, {
        label: 'ค่าน้ำประปา',
        backgroundColor: "#059BFF",
        data: waterData
      }, {
        label: 'ค่าไฟฟ้า',
        backgroundColor: "#FFC233",
        data: electData
      }]
    },
    "options": {
      "legend": {
        "display": false
      },
      "tooltips": {
        "mode": 'index',
        "intersect": false
      },
      "scales": {
        "xAxes": [{
          "stacked": true,
        }],
        "yAxes": [{
          "stacked": true,
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  })
}

function genCor() {
  $(`#corChart`).empty()
  var cor = document.getElementById("corChart").getContext('2d')
  var corChart = new Chart(cor, {
    "type": "bar",
    "data": {
      labels: month,
      datasets: [{
        label: 'ค่าเช่าสถานที่',
        backgroundColor: "#FF3D67",
        data: corData
      }, {
        label: 'ค่าใช้จ่ายอื่น ๆ',
        backgroundColor: "#059BFF",
        data: otherData
      }],
    },
    "options": {
      "legend": {
        "display": false
      },
      "tooltips": {
        "mode": 'index',
        "intersect": false
      },
      "scales": {
        "xAxes": [{
          "stacked": true,
        }],
        "yAxes": [{
          "stacked": true,
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  })
}

function genCol() {
  $(`#colChart`).empty()
  var col = document.getElementById("colChart").getContext('2d')
  var colChart = new Chart(col, {
    "type": "bar",
    "data": {
      "labels": month,
      "datasets": [{
        "label": "ต้นทุนค่าจ้างพนักงาน",
        "data": colData,
        "fill": false,
        "backgroundColor": ["rgb(255, 61, 103)", "rgb(255, 194, 51)", "rgb(5, 155, 255)", "rgb(29,233,182)"],
        "lineTension": 0.1
      }]
    },
    "options": {
      "legend": {
        "display": false
      },
      "scales": {
        "yAxes": [{
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  })
}

function genProfit() {
  $(`#profitChart`).empty()
  var profit = document.getElementById("profitChart").getContext('2d')
  var profitChart = {
    labels: month,
    datasets: [{
      label: 'ต้นทุนการผลิตรวม',
      borderColor: '#ff6283',
      fill: false,
      data: allCost,
      lineTension: 0.1
    }, {
      label: 'ยอดขายรวม',
      borderColor: `#17a1e6`,
      fill: false,
      data: profitData,
      lineTension: 0.1
    }]
  }
  window.myLine = Chart.Line(profit, {
    data: profitChart,
    options: {
      responsive: true,
      hoverMode: 'index',
      stacked: false,
      "scales": {
        "yAxes": [{
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  })
}

function genSuggestion() {
  firebase.database().ref(`stores/${store}/storeType`).on(`value`, snapshot => {
    getProfitSuggestion(snapshot.val())
    getCosSuggestion(snapshot.val())
    getColSuggestion(snapshot.val())
    getCorSuggestion(snapshot.val())
  })
}

function getProfitSuggestion (type) {
  if (type == `1`) {
    var costRecommend = (profitData[3] * 0.7)
    var costPercentage = costRecommend - allCost[3]
    if (costPercentage < 0) {
      var profitRecommend = (costPercentage * -1) / (costRecommend * 0.01)
      $(`#profit-suggestion`).html(`ผลประกอบการอยู่ในระดับที่น่าเป็นห่วงเนื่องจากมีต้นทุนการผลิตสูงกว่าที่ควรจะเป็นอยู่ ${parseInt(profitRecommend)}% ควรทำยอดขายเพิ่มขึ้นอย่างน้อย ${formatMoney(parseFloat(allCost[3] / 0.7) - profitData[3])} บาท เพื่อให้ผลประกอบการอยู่ในระดับที่ปลอดภัย`)
    } else if (costPercentage => 0) {
      var profitRecommend = costPercentage / (costRecommend * 0.01)
      $(`#profit-suggestion`).html(`ผลประกอบการอยู่ในระดับที่น่าพึ่งพอใจเนื่องจากมีต้นทุนการผลิตต่ำกว่าที่ควรจะเป็นอยู่ ${parseInt(profitRecommend)}% ทำให้มีกำไรสูงกว่าระดับที่ปลอดภัยอยู่ ${formatMoney((parseFloat(allCost[3] / 0.7) - profitData[3]) * -1)} บาท`)
    }
  } else if (type == `2`) {
    var costRecommend = (profitData[3] * 0.6)
    var costPercentage = costRecommend - allCost[3]
    if (costPercentage < 0) {
      var profitRecommend = (costPercentage * -1) / (costRecommend * 0.01)
      $(`#profit-suggestion`).html(`ผลประกอบการอยู่ในระดับที่น่าเป็นห่วงเนื่องจากมีต้นทุนการผลิตสูงกว่าที่ควรจะเป็นอยู่ ${parseInt(profitRecommend)}% ควรทำยอดขายเพิ่มขึ้นอย่างน้อย ${formatMoney(parseFloat(allCost[3] / 0.7) - profitData[3])} บาท เพื่อให้ผลประกอบการอยู่ในระดับที่ปลอดภัย`)
    } else if (costPercentage => 0) {
      var profitRecommend = costPercentage / (costRecommend * 0.01)
      $(`#profit-suggestion`).html(`ผลประกอบการอยู่ในระดับที่น่าพึ่งพอใจเนื่องจากมีต้นทุนการผลิตต่ำกว่าที่ควรจะเป็นอยู่ ${parseInt(profitRecommend)}% ทำให้มีกำไรสูงกว่าระดับที่ปลอดภัยอยู่ ${formatMoney((parseFloat(allCost[3] / 0.7) - profitData[3]) * -1)} บาท`)
    }
  }
}

function getCosSuggestion (type) {
  if (type == `1`) {
    var costRecommend = (profitData[3] * 0.35)
    var costPercentage = costRecommend - (cosData[3] + electData[3] + waterData[3])
    if (costPercentage < 0) {
      var cosRecommend = (costPercentage * -1) / (costRecommend * 0.01)
      $(`#cos-suggestion`).html(`ต้นทุนค่าวัตถุดิบอยู่ในระดับที่น่าเป็นห่วงเนื่องจากมีต้นทุนสูงกว่าที่ควรจะเป็นอยู่ ${parseInt(cosRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat(cosData[3] + electData[3] + waterData[3]) - costRecommend)} บาท`)
    } else if (costPercentage => 0) {
      var cosRecommend = costPercentage / (costRecommend * 0.01)
      $(`#cos-suggestion`).html(`ต้นทุนค่าวัตถุดิบอยู่ในระดับที่น่าพึงพอใจเนื่องจากมีต้นทุนต่ำกว่าที่ควรจะเป็นอยู่ ${parseInt(cosRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat((cosData[3] + electData[3] + waterData[3]) - costRecommend) * -1)} บาท`)
    }
  } else if (type == `2`) {
    var costRecommend = (profitData[3] * 0.25)
    var costPercentage = costRecommend - (cosData[3] + electData[3] + waterData[3])
    if (costPercentage < 0) {
      var cosRecommend = (costPercentage * -1) / (costRecommend * 0.01)
      $(`#cos-suggestion`).html(`ต้นทุนค่าวัตถุดิบอยู่ในระดับที่น่าเป็นห่วงเนื่องจากมีต้นทุนสูงกว่าที่ควรจะเป็นอยู่ ${parseInt(cosRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat(cosData[3] + electData[3] + waterData[3]) - costRecommend)} บาท`)
    } else if (costPercentage => 0) {
      var cosRecommend = costPercentage / (costRecommend * 0.01)
      $(`#cos-suggestion`).html(`ต้นทุนค่าวัตถุดิบอยู่ในระดับที่น่าพึงพอใจเนื่องจากมีต้นทุนต่ำกว่าที่ควรจะเป็นอยู่ ${parseInt(cosRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat((cosData[3] + electData[3] + waterData[3]) - costRecommend) * -1)} บาท`)
    }
  }
}

function getColSuggestion (type) {
  if (type == `1`) {
    var costRecommend = (profitData[3] * 0.25)
    var costPercentage = costRecommend - colData[3]
    if (costPercentage < 0) {
      var colRecommend = (costPercentage * -1) / (costRecommend * 0.01)
      $(`#col-suggestion`).html(`ต้นทุนค่าจ้างพนักงานอยู่ในระดับที่น่าเป็นห่วงเนื่องจากมีต้นทุนสูงกว่าที่ควรจะเป็นอยู่ ${parseInt(colRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat(colData[3]) - costRecommend)} บาท`)
    } else if (costPercentage => 0) {
      var colRecommend = costPercentage / (costRecommend * 0.01)
      $(`#col-suggestion`).html(`ต้นทุนค่าจ้างพนักงานอยู่ในระดับที่น่าพึงพอใจเนื่องจากมีต้นทุนต่ำกว่าที่ควรจะเป็นอยู่ ${parseInt(colRecommend)}% คิดเป็นเงิน ${formatMoney((parseFloat(colData[3]) - costRecommend) * -1)} บาท`)
    }
  } else if (type == `2`) {
    var costRecommend = (profitData[3] * 0.25)
    var costPercentage = costRecommend - colData[3]
    if (costPercentage < 0) {
      var colRecommend = (costPercentage * -1) / (costRecommend * 0.01)
      $(`#col-suggestion`).html(`ต้นทุนค่าจ้างพนักงานอยู่ในระดับที่น่าเป็นห่วงเนื่องจากมีต้นทุนสูงกว่าที่ควรจะเป็นอยู่ ${parseInt(colRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat(colData[3]) - costRecommend)} บาท`)
    } else if (costPercentage => 0) {
      var colRecommend = costPercentage / (costRecommend * 0.01)
      $(`#col-suggestion`).html(`ต้นทุนค่าจ้างพนักงานอยู่ในระดับที่น่าพึงพอใจเนื่องจากมีต้นทุนต่ำกว่าที่ควรจะเป็นอยู่ ${parseInt(colRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat((colData[3]) - costRecommend) * -1)} บาท`)
    }
  }
}

function getCorSuggestion (type) {
  if (type == `1`) {
    var costRecommend = (profitData[3] * 0.20)
    var costPercentage = costRecommend - (corData[3] + otherData[3])
    if (costPercentage < 0) {
      var corRecommend = (costPercentage * -1) / (costRecommend * 0.01)
      $(`#cor-suggestion`).html(`ต้นทุนค่าเช่าสถานที่และค่าใช้จ่ายอื่น ๆ อยู่ในระดับที่น่าเป็นห่วงเนื่องจากมีต้นทุนสูงกว่าที่ควรจะเป็นอยู่ ${parseInt(corRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat(corData[3] + otherData[3]) - costRecommend)} บาท`)
    } else if (costPercentage => 0) {
      var corRecommend = costPercentage / (costRecommend * 0.01)
      $(`#cor-suggestion`).html(`ต้นทุนค่าเช่าสถานที่และค่าใช้จ่ายอื่น ๆ อยู่ในระดับที่น่าพึงพอใจเนื่องจากมีต้นทุนต่ำกว่าที่ควรจะเป็นอยู่ ${parseInt(corRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat((corData[3] + otherData[3]) - costRecommend) * -1)} บาท`)
    }
  } else if (type == `2`) {
    var costRecommend = (profitData[3] * 0.20)
    var costPercentage = costRecommend - (cosData[3] + electData[3] + waterData[3])
    if (costPercentage < 0) {
      var corRecommend = (costPercentage * -1) / (costRecommend * 0.01)
      $(`#cor-suggestion`).html(`ต้นทุนค่าเช่าสถานที่และค่าใช้จ่ายอื่น ๆ อยู่ในระดับที่น่าเป็นห่วงเนื่องจากมีต้นทุนสูงกว่าที่ควรจะเป็นอยู่ ${parseInt(corRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat(corData[3] + otherData[3]) - costRecommend)} บาท`)
    } else if (costPercentage => 0) {
      var corRecommend = costPercentage / (costRecommend * 0.01)
      $(`#cor-suggestion`).html(`ต้นทุนค่าเช่าสถานที่และค่าใช้จ่ายอื่น ๆ อยู่ในระดับที่น่าพึงพอใจเนื่องจากมีต้นทุนต่ำกว่าที่ควรจะเป็นอยู่ ${parseInt(corRecommend)}% คิดเป็นเงิน ${formatMoney(parseFloat((corData[3] + otherData[3]) - costRecommend) * -1)} บาท`)
    }
  }
}

const formatMoney = (num) => {
  if (num !== ``) {
    var p = num.toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
  } else {
    return `0.00`
  }
}
