$(document).ready(() => {
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(document).height() - 56)
  }

  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }
})

const createList = (role, data, key) => {
  if (role === 'owner') {
    var txt = `<div class="col-lg-8 col-12 mx-auto store__name mb-3" id="` + key + `" onclick="window.location.href='./store-menu.html?store=` + key + `&role=owner'">
                <div class="row m-0">
                  <div class="col-12 text-right p-0">รหัสร้าน
                    <span class="font-weight-bold display-5">` + data['storeCode'] + `</span>
                  </div>
                  <div class="col-12 text-center py-2">
                    <h2 class="m-0">` + data['storeName'] + `</h2>
                  </div>
                </div>
              </div>`
    $('#store-list').append(txt)
  } else if (role === 'manager') {
    var txt = `<div class="col-lg-8 col-12 mx-auto store__name mb-3" id="` + key + `" onclick="window.location.href='./update-sales.html?store=` + key + `&role=manager'">
                <div class="row m-0">
                  <div class="col-12 text-right p-0">รหัสร้าน
                    <span class="font-weight-bold display-5">` + data['storeCode'] + `</span>
                  </div>
                  <div class="col-12 text-center py-2">
                    <h2 class="m-0">` + data['storeName'] + `</h2>
                  </div>
                </div>
              </div>`
    $('#store-list').append(txt)
  } else if (role === 'staffer') {
    var txt = `<div class="col-lg-8 col-12 mx-auto store__name mb-3" id="` + key + `" onclick="window.location.href='./update-sales.html?store=` + key + `&role=staffer'">
                <div class="row m-0">
                  <div class="col-12 text-right p-0">รหัสร้าน
                    <span class="font-weight-bold display-5">` + data['storeCode'] + `</span>
                  </div>
                  <div class="col-12 text-center py-2">
                    <h2 class="m-0">` + data['storeName'] + `</h2>
                  </div>
                </div>
              </div>`
    $('#store-list').append(txt)
  }
}

const onlineWorker = () => {
  $('#store-list').empty()
  $('#create-store-btn').removeClass('store__add_block')
  $('#create-store-btn').addClass('store__add')
  $("#create-store-btn").attr('onclick', "window.location.href='./create-store.html'")
  $('#profile-link-md').attr('href', './edit-profile.html')
  $('#profile-link-md').removeClass('text-muted')
  $('#search-link-md').attr('href', './search-store.html')
  $('#search-link-md').removeClass('text-muted')
  $('#access-alert-md').attr('href', './access-request.html')
  $('#access-alert-md').removeClass('text-muted')
  $('#profile-link').attr('href', './edit-profile.html')
  $('#profile-link').removeClass('text-muted')
  $('#search-link').attr('href', './search-store.html')
  $('#search-link').removeClass('text-muted')
  $('#access-alert').attr('href', './access-request.html')
  $('#access-alert').removeClass('text-muted')

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser
      localforage.getItem('users', (err, userData) => {
        $('#show_name').text(userData[String(user.uid)]['fullname'])
        $('#show_name_md').text(userData[String(user.uid)]['fullname'])
      })
      firebase.database().ref('stores').orderByChild('userList/' + user.uid).equalTo('owner').on('value', snapshot => {
        for (var key in snapshot.val()) {
          $(`#${key}`).remove()
          createList('owner', snapshot.val()[key], key)
        }
        localforage.setItem('storesOwner', snapshot.val())
      })
      firebase.database().ref('stores').orderByChild('userList/' + user.uid).equalTo('manager').on('value', snapshot => {
        for (var key in snapshot.val()) {
          $(`#${key}`).remove()
          createList('manager', snapshot.val()[key], key)
        }
        localforage.setItem('storesManager', snapshot.val())
      })
      firebase.database().ref('stores').orderByChild('userList/' + user.uid).equalTo('staffer').on('value', snapshot => {
        for (var key in snapshot.val()) {
          $(`#${key}`).remove()
          createList('staffer', snapshot.val()[key], key)
        }
        localforage.setItem('storesStaffer', snapshot.val())
      })
      firebase.database().ref(`stores`).orderByChild(`userList/${user.uid}`).equalTo(`owner`).on(`value`, snapshot => {
        if (snapshot.val() != null) {
          for (var key in snapshot.val()) {
            firebase.database().ref(`stores/${key}/accessRequest`).on(`value`, snapshot => {
              if (snapshot.val() != null) {
                $(`#notificationCount`).html(snapshot.numChildren())
                $(`#notificationCount-md`).html(snapshot.numChildren())
                $('#access-alert').addClass('text-danger font-weight-bold')
                $('#access-alert-md').addClass('text-danger font-weight-bold')
              }
            })
          }
        }
      })
    } else {
      window.location.href = `./index.html`
    }
  })
}

const offlineWorker = () => {
  $('#create-store-btn').removeAttr('onclick')
  $('#store-list').empty()
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser
      localforage.getItem('users', (err, userData) => {
        $('#show_name').text(userData[String(user.uid)]['fullname'])
        $('#show_name_md').text(userData[String(user.uid)]['fullname'])
      })
      localforage.getItem('storesOwner', (err, storeData) => {
        var i = 0
        for (var key in storeData) {
          if (storeData.hasOwnProperty(key)) {
            var keys = Object.keys(storeData)
            RegExp.escape = function (s) {
              return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            };
            var id = new RegExp("[" + user.uid + "]")
            if (id.test(key)) {
              createList('owner', storeData[key], keys[i])
            }
            var refAccess = firebase.database().ref("stores/" + keys[i] + "/accessRequest")
            refAccess.on('child_added', (snapshot) => {
              $('#access-alert').addClass('text-danger font-weight-bold')
              $('#access-alert-md').addClass('text-danger font-weight-bold')
            })
            refAccess.on('child_removed', (snapshot) => {
              $('#access-alert').removeClass('text-danger font-weight-bold')
              $('#access-alert-md').removeClass('text-danger font-weight-bold')
            })
          }
          i++
        }
      })
      localforage.getItem('storesManager', (err, storeData) => {
        var i = 0
        for (var key in storeData) {
          if (storeData.hasOwnProperty(key)) {
            var keys = Object.keys(storeData)
            RegExp.escape = function (s) {
              return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            };
            var id = new RegExp("[" + user.uid + "]")
            if (id.test(key)) {
              createList('manager', storeData[key], keys[i])
            }
          }
          i++
        }
      })
      localforage.getItem('storesStaffer', (err, storeData) => {
        var i = 0
        for (var key in storeData) {
          if (storeData.hasOwnProperty(key)) {
            var keys = Object.keys(storeData)
            RegExp.escape = function (s) {
              return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            };
            var id = new RegExp("[" + user.uid + "]")
            if (id.test(key)) {
              createList('staffer', storeData[key], keys[i])
            }
          }
          i++
        }
      })
    } else {
      window.location.href = `./index.html`
    }
  })
  $('#create-store-btn').removeAttr('onclick')
  $('#create-store-btn').addClass('store__add_block')
  $('#profile-link-md').removeAttr('href')
  $('#profile-link-md').addClass('text-muted')
  $('#search-link-md').removeAttr('href')
  $('#search-link-md').addClass('text-muted')
  $('#access-alert-md').removeAttr('href')
  $('#access-alert-md').addClass('text-muted')
  $('#profile-link').removeAttr('href')
  $('#profile-link').addClass('text-muted')
  $('#search-link').removeAttr('href')
  $('#search-link').addClass('text-muted')
  $('#access-alert').removeAttr('href')
  $('#access-alert').addClass('text-muted')
}

window.addEventListener('online',  ()=>{
  onlineWorker()
})

window.addEventListener('offline', ()=>{
  offlineWorker()
})

window.onresize = (event)=>{
  $('.row-menu').css('height', $(window).height() - 56)
}