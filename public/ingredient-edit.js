var storeName
var storeName
var url = new URL(window.location.href)
var store = url.searchParams.get('store')
var role = url.searchParams.get('role')
var poID = url.searchParams.get(`po`)
var countRow = 0
var proponent
var d = new Date()

$(document).ready(() => {
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(window).height() - 56)
    $('.row-menu').css('mex-height', $(window).height() - 56)
  }
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  var d = new Date()
  var toDay = `${d.getFullYear()}-${(`0` + (d.getMonth() + 1)).slice(-2)}-${(`0` + d.getDate()).slice(-2)}`
  $(`#docDate`).val(toDay)
  $(`#deliveryDate`).attr(`min`, toDay)

  $(`#navbar-text`).append(`ใบสั่งซื้อเลขที่ ${poID}`)
  firebase.database().ref(`stores/${store}/bills/${poID}`).once('value', snapshot => {
    firebase.database().ref(`users/${snapshot.val()[`proponent`]}/fullname`).on(`value`, snapshot => {
      $(`#poProponent`).append(`ผู้เสนอใบสั่งซื้อ : ${snapshot.val()}`)
    })
    proponent = snapshot.val()[`proponent`]
    $(`#PONumber`).val(poID)
    $(`#deliveryDate`).val(snapshot.val()[`dateDelivery`])
    $(`#contactPerson`).val(snapshot.val()[`supplier`][`contact`])
    $(`#tel`).val(snapshot.val()[`supplier`][`tel`])
    $(`#fax`).val(snapshot.val()[`supplier`][`fax`])
    $(`#email`).val(snapshot.val()[`supplier`][`email`])
    $(`#address`).val(snapshot.val()[`supplier`][`address`].replace(/<br>/g, '\n'))
    $(`#remarksContact`).val(snapshot.val()[`supplier`][`remarks`].replace(/<br>/g, '\n'))
    $(`#subtotal`).val(snapshot.val()[`product`][`subtotal`])
    $(`#payment`).val(snapshot.val()[`product`][`paymentMethod`])
    $(`#discount`).val(snapshot.val()[`product`][`discount`])
    $(`#paymentTerms`).val(snapshot.val()[`product`][`paymentTerm`].replace(/<br>/g, '\n'))
    $(`#remarks`).val(snapshot.val()[`product`][`remarks`].replace(/<br>/g, '\n'))
    $(`#total`).val(snapshot.val()[`total`])
    if (snapshot.val()[`statusRemarks`] != null) {
      $(`#statusRemarks`).val(snapshot.val()[`statusRemarks`].replace(/<br>/g, '\n'))
    }
    var txt = ``
    for (var i = 0; i < snapshot.val()[`product`][`list`].length; i++) {
      txt += `<tr id="${countRow}" onclick="actionList($(this))">
                <td class="td-list">${snapshot.val()[`product`][`list`][i][`item`]}</td>
                <td class="td-quantity">${snapshot.val()[`product`][`list`][i][`quantity`]}</td>
                <td class="td-unit">${snapshot.val()[`product`][`list`][i][`unit`]}</td>
                <td class="td-price">${snapshot.val()[`product`][`list`][i][`price`]}</td>
                <td class="td-discount">${snapshot.val()[`product`][`list`][i][`discount`]}</td>
                <td class="td-total">${snapshot.val()[`product`][`list`][i][`total`]}</td>
              </tr>`
      countRow++
    }
    $(`#bill-table tbody`).append(txt)
    $(`#table-empty-alert`).empty()
    $(`#help-table`).removeClass(`d-none`)
    $('#bill-table tbody .td-total').each(function () {
      price += parseFloat($(this).html().replace(/,/g, ''))
    })
    if (snapshot.val()[`product`][`tax`][`type`] === `novat`) {
      $(`#vat`).val(`novat`)
    } else if (snapshot.val()[`product`][`tax`][`type`] === `excluded`){
      $(`#vat`).val(`excluded`)
      $(`#excluded`).attr(`data-exvat`, snapshot.val()[`product`][`tax`][`value`])
    } else if (snapshot.val()[`product`][`tax`][`type`] === `included`){
      $(`#vat`).val(`included`)
      $(`#included`).attr(`data-invat`, snapshot.val()[`product`][`tax`][`value`])
    }
    calTax()
    if (snapshot.val()[`status`] == `awaiting`) {
      $(`#status`).val(`awaiting`)
      $(`#status-color`).addClass(`bg-awaiting`)
    } else if (snapshot.val()[`status`] == `complete`) {
      $(`#status`).val(`complete`)
      $(`#status-color`).addClass(`bg-complete`)
    } else if (snapshot.val()[`status`] == `fail`) {
      $(`#status`).val(`fail`)
      $(`#status-color`).addClass(`bg-fail`)
    }
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        var user = firebase.auth().currentUser
        if (user.uid === snapshot.val()[`proponent`]) {
          $(`#back-btn`).attr(`onclick`, `toRequest("creator")`)
          $(`#finish-btn`).attr(`onclick`, `toRequest("creator")`)
          $(`#poProponent`).remove()
          $(`hr`).remove()
          $(`#update-status`).hide()
          var txt = ``
          if (snapshot.val()[`status`] === `complete`) {
            $(`#show-status`).append(`<h5 class="mt-4 mx-2">สถานะใบสั่งซื้อ : อนุมัติ</h5>`)
          } else if (snapshot.val()[`status`] === `awaiting`) {
            $(`#show-status`).append(`<h5 class="mt-4 mx-2">สถานะใบสั่งซื้อ : รอการประเมิน</h5>`)
          } else if (snapshot.val()[`status`] === `fail`) {
            $(`#show-status`).append(`<h5 class="mt-4 mx-2">สถานะใบสั่งซื้อ : ไม่อนุมัติ</h5>`)
          }
        } else {
          $(`#show-status`).hide()
          $(`#finish-btn`).attr(`onclick`, `toRequest("viewer")`)
          $(`#back-btn`).attr(`onclick`, `toRequest("viewer")`)
          $(`hr`).removeClass(`d-none`)
        }
      }
    })
  })
})

$('#delConfirm').keyup(() => {
  if ($('#delConfirm').val() !== storeName) {
    $('#delConfirm-btn').prop('disabled', true)
  } else {
    $('#delConfirm-btn').prop('disabled', false)
  }
})

$('#delConfirm-btn').click(() => {
  firebase.database().ref('stores/' + store).remove().then(() => {
    window.location.href = './store-list.html'
  }).catch((error) => {
    console.log(error)
  })
})

window.onresize = (event) => {
  $('.row-menu').css('height', $(window).height() - 56)
}

window.addEventListener('online', () => {
  onlineWorker()
})

window.addEventListener('offline', () => {
  offlineWorker()
})

const onlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
    var store = url.searchParams.get('store')
    var role = url.searchParams.get('role')
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        if (role === 'owner') {
          localforage.getItem('storesOwner', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="other">
                        <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                          <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=owner">
                          <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-request.html?store=${store}&role=owner">
                          <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=owner">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./expenses.html?store=${store}&role=owner">
                          <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./edit-access.html?store=${store}&role=owner">
                          <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./create-menulist.html?store=${store}&role=owner">
                          <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./edit-store.html?store=${store}&role=owner">
                          <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                        </a>
                      </li>
                      <li class="del_store">
                        <a role="button" href="#" data-toggle="modal" data-target="#delStore">
                          <i class="material-icons">delete</i> ลบร้านค้า
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        } else if (role === 'manager') {
          localforage.getItem('storesManager', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="other">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=manager">
                          <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-request.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=manager">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-assessment.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./expenses.html?store=${store}&role=manager">
                          <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        } else if (role === 'staffer') {
          window.location.href = './update-sales.html?store=' + store + '&role=staffer'
        }
        firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
          $('#delStoreName').text(snapshot.val())
        })
      } else {
        window.location.href = './index.html'
      }
    })
}

const offlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                        <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                      </a>
                    </li>
                    <li class="other">
                      <a role="button" href="#" class="text-muted">
                        <i class="material-icons">delete</i> ลบร้านค้า
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        localforage.getItem('storesManager', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'staffer') {
        window.location.href = './update-sales.html?store=' + store + '&role=staffer'
      }
    } else {
      window.location.href = './index.html'
    }
  })
}

const inputNumber = (e) => {
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode >= 35 && e.keyCode <= 39)) {
    return;
  }
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
}

$(`#btn-add-bill`).click(() => {
  $(`#addList`).modal()
})

var countRow = 0
var price = 0.00

$(`#confirm-addList`).click(() => {
  $(`#confirm-addList`).prop(`disabled`, true)
  if ($(`#productDiscount`).val() === ``) {
    $(`#productDiscount`).val(`0`)
  }
  var formList = $(`#addList-form`).serializeArray()
  document.getElementById(`addList-form`).reset()
  $(`#addList`).modal(`hide`)
  $(`#help-table`).removeClass(`d-none`)
  $(`#table-empty-alert`).empty()
  var total
  if (formList[4][`value`] !== ``) {
    total = (formList[1][`value`] * formList[3][`value`]) - (formList[1][`value`] * formList[4][`value`])
  } else {
    total = (formList[1][`value`] * formList[3][`value`])
  }
  var txt = `<tr id="${countRow}" onclick="actionList($(this))">
              <td class="td-list">${formList[0][`value`]}</td>
              <td class="td-quantity">${formList[1][`value`]}</td>
              <td class="td-unit">${formList[2][`value`]}</td>
              <td class="td-price">${formatMoney(parseFloat(formList[3][`value`]))}</td>
              <td class="td-discount">${formatMoney(parseFloat(formList[4][`value`]))}</td>
              <td class="td-total">${formatMoney(parseFloat(total))}</td>
            </tr>`
  $(`#bill-table tbody`).append(txt)
  price = 0.00
  $('#bill-table tbody .td-total').each(function () {
    price += parseFloat($(this).html().replace(/,/g, ''))
  })
  $(`#subtotal`).val(formatMoney(price))
  calTax()
  countRow++
})

const actionList = (elem) => {
  $(`#delList-btn`).attr(`data-target`, elem.attr(`id`))
  $(`#editList-btn`).attr(`data-target`, elem.attr(`id`))
  $(`#editListTemp`).val(elem.children(":nth-child(1)").html())
  $(`#editQuantityTemp`).val(elem.children(":nth-child(2)").html())
  $(`#editUnitTemp`).val(elem.children(":nth-child(3)").html())
  $(`#editPriceTemp`).val(elem.children(":nth-child(4)").html())
  $(`#editDiscountTemp`).val(elem.children(":nth-child(5)").html())

  $(`#actionProduct`).text(elem.children(":nth-child(1)").html())
  $(`#actionQuantity`).text(elem.children(":nth-child(2)").html())
  $(`#actionUnit`).text(elem.children(":nth-child(3)").html())
  $(`#actionUnit-2`).text(elem.children(":nth-child(3)").html())
  $(`#actionPrice`).text(elem.children(":nth-child(4)").html())
  $(`#actionTotal`).text(elem.children(":nth-child(6)").html())
  $(`#editRow`).modal()
}

$(`#delList-btn`).click(() => {
  var removePrice = $(`#bill-table tbody #${$(`#delList-btn`).attr(`data-target`)}`).children(":nth-child(6)").html()
  price = price - (parseFloat(removePrice.replace(/,/g, '')))
  $(`#subtotal`).val(formatMoney(price))
  $(`#bill-table tbody #${$(`#delList-btn`).attr(`data-target`)}`).remove()
  $(`#editRow`).modal(`hide`)
  if ($(`#bill-table tbody tr`).length <= 0) {
    $(`#help-table`).addClass(`d-none`)
    $(`#table-empty-alert`).append(`<h2 class="text-muted mt-2">ยังไม่มีรายการสินค้า</h2>`)
    $(`#val`).val(`0.00`)
    $(`#subtotal`).val(`0.00`)
    $(`#total`).val(`0.00`)
  }
  calTax()
})

$(`#editList-btn`).click(() => {
  $(`#confirm-saveList`).attr(`data-target`, $(`#editList-btn`).attr(`data-target`))
  $(`#editProductList`).val($(`#editListTemp`).val())
  $(`#editProductQuantity`).val($(`#editQuantityTemp`).val())
  $(`#editProductUnit`).val($(`#editUnitTemp`).val())
  $(`#editProductPrice`).val($(`#editPriceTemp`).val())
  $(`#editProductDiscount`).val($(`#editDiscountTemp`).val())
  $(`#editRow`).modal(`hide`)
  $(`#editList`).modal()
})

$(`#confirm-saveList`).click(() => {
  $(`#bill-table tbody #${$(`#confirm-saveList`).attr(`data-target`)}`).empty()
  var editList = $(`#saveList-form`).serializeArray()
  var total
  if (editList[4][`value`] !== ``) {
    total = (editList[1][`value`] * editList[3][`value`]) - (editList[1][`value`] * editList[4][`value`])
  } else {
    total = (editList[1][`value`] * editList[3][`value`])
  }
  var txt = `<td class="td-list">${editList[0][`value`]}</td>
              <td class="td-quantity">${editList[1][`value`]}</td>
              <td class="td-unit">${editList[2][`value`]}</td>
              <td class="td-price">${formatMoney(parseFloat(editList[3][`value`]))}</td>
              <td class="td-discount">${formatMoney(parseFloat(editList[4][`value`]))}</td>
              <td class="td-total">${formatMoney(parseFloat(total))}</td>`
  $(`#bill-table tbody #${$(`#confirm-saveList`).attr(`data-target`)}`).append(txt)
  $(`#editList`).modal(`hide`)
  price = 0.00
  $('#bill-table tbody .td-total').each(function () {
    price += parseFloat($(this).html().replace(/,/g, ''))
  })
  $(`#subtotal`).val(formatMoney(price))
  calTax()
})

const formatMoney = (num) => {
  if (num !== ``) {
    var p = num.toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function (acc, num, i, orig) {
      return num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
  } else {
    return `0.00`
  }
}

$('#productList').keyup(() => {
  if ($(`#productList`).val() !== `` && $(`#productQuantity`).val() !== `` && $(`#productUnit`).val() !== `` && $(`#productPrice`).val() !== ``) {
    $(`#confirm-addList`).prop(`disabled`, false)
  }
})

$('#productQuantity').keyup(() => {
  if ($(`#productList`).val() !== `` && $(`#productQuantity`).val() !== `` && $(`#productUnit`).val() !== `` && $(`#productPrice`).val() !== ``) {
    $(`#confirm-addList`).prop(`disabled`, false)
  }
})

$('#productUnit').keyup(() => {
  if ($(`#productList`).val() !== `` && $(`#productQuantity`).val() !== `` && $(`#productUnit`).val() !== `` && $(`#productPrice`).val() !== ``) {
    $(`#confirm-addList`).prop(`disabled`, false)
  }
})

$('#productPrice').keyup(() => {
  if ($(`#productList`).val() !== `` && $(`#productQuantity`).val() !== `` && $(`#productUnit`).val() !== `` && $(`#productPrice`).val() !== ``) {
    $(`#confirm-addList`).prop(`disabled`, false)
  }
})

$('#addList-form').on('keyup', (e) => {
  var keyCode = e.keyCode
  if (keyCode === 13) {
    $(`#confirm-addList`).click()
  }
})

$('#editProductList').keyup(() => {
  if ($(`#editProductList`).val() !== `` && $(`#editProductQuantity`).val() !== `` && $(`#editProductUnit`).val() !== `` && $(`#editProductPrice`).val() !== ``) {
    $(`#confirm-saveList`).prop(`disabled`, false)
  }
})

$('#editProductQuantity').keyup(() => {
  if ($(`#editProductList`).val() !== `` && $(`#editProductQuantity`).val() !== `` && $(`#editProductUnit`).val() !== `` && $(`#editProductPrice`).val() !== ``) {
    $(`#confirm-saveList`).prop(`disabled`, false)
  }
})

$('#editProductUnit').keyup(() => {
  if ($(`#editProductList`).val() !== `` && $(`#editProductQuantity`).val() !== `` && $(`#editProductUnit`).val() !== `` && $(`#editProductPrice`).val() !== ``) {
    $(`#confirm-saveList`).prop(`disabled`, false)
  }
})

$('#editProductPrice').keyup(() => {
  if ($(`#editProductList`).val() !== `` && $(`#editProductQuantity`).val() !== `` && $(`#editProductUnit`).val() !== `` && $(`#editProductPrice`).val() !== ``) {
    $(`#confirm-saveList`).prop(`disabled`, false)
  }
})

$('#saveList-form').on('keyup', (e) => {
  var keyCode = e.keyCode
  if (keyCode === 13) {
    $(`#confirm-saveList`).click()
  }
})

$(`#discount`).blur(function () {
  if ($(this).val() != 0 && $(this).val() != null) {
    var discountTemp = parseFloat(($(this).val()).replace(/,/g, ''))
    $(this).prop(`type`, `text`)
    $(this).val(formatMoney(discountTemp))
    var totalTemp = parseFloat($(`#total`).val().replace(/,/g, ''))
    if ((totalTemp - discountTemp) > 0) {
      $(`#total`).val(formatMoney(totalTemp - discountTemp))
    }
    calTax()
  } else if ($(this).val().length == 0) {
    $(this).val(`0.00`)
    calTax()
  }
})

$(`#discount`).focus(function () {
  if ($(this).val() != 0 && $(this).val() != null) {
    var discountTemp = parseFloat(($(this).val()).replace(/,/g, ''))
    $(this).val(discountTemp)
    $(this).prop(`type`, `number`)
    calTax()
  }
})

$(`#vat`).change(()=>{
  calTax()
})

const calTax = () => {
  $(`#excluded`).empty()
  $(`#included`).empty()
  var discount = parseFloat($(`#discount`).val().replace(/,/g, ''))
  if ((price - discount) >= 0) {
    var excludedVat = (price - discount) * 0.07
    var includedVat = (price - discount) - ((price - discount) * (100/107))
    $(`#excluded`).html(`ภาษีแยก จำนวน ${formatMoney(excludedVat)} บาท`)
    $(`#included`).html(`ภาษีรวม จำนวน ${formatMoney(includedVat)} บาท`)
    $(`#excluded`).attr(`data-exvat`, formatMoney(excludedVat))
    $(`#included`).attr(`data-invat`, formatMoney(includedVat))
    if ($(`#vat`).val() === `novat`) {
      $(`#subtotal`).val(formatMoney(price))
      $(`#total`).val(formatMoney(price - discount))
    } else if ($(`#vat`).val() === `excluded`) {
      $(`#subtotal`).val(formatMoney(price))
      $(`#total`).val(formatMoney((price - discount) + excludedVat))
    } else if ($(`#vat`).val() === `included`) {
      $(`#subtotal`).val(formatMoney(price - includedVat))
      $(`#total`).val(formatMoney(price - discount))
    }
  }
}

$(`#status`).change(function () {
  if ($(this).val() === `awaiting`) {
    $(`#status-color`).removeClass(`bg-complete`)
    $(`#status-color`).addClass(`bg-awaiting`)
    $(`#status-color`).removeClass(`bg-fail`)
  } else if ($(this).val() === `complete`) {
    $(`#status-color`).addClass(`bg-complete`)
    $(`#status-color`).removeClass(`bg-awaiting`)
    $(`#status-color`).removeClass(`bg-fail`)
  } else if ($(this).val() === `fail`) {
    $(`#status-color`).removeClass(`bg-complete`)
    $(`#status-color`).removeClass(`bg-awaiting`)
    $(`#status-color`).addClass(`bg-fail`)
  }
})

$(`#saveBill-btn`).click(() => {
  var jsonTable = $('#bill-table').tableToJSON()
  var len = jsonTable.length
  var data = `{`
  for (var i = 0; i < len; i++) {
    data += `"${i}":${JSON.stringify(jsonTable[i])}`
    if (i != len - 1) {
      data += `,`
    } else {
      data += `}`
    }
  }
  firebase.auth().onAuthStateChanged((user) => {
    var allDataObj = `{"${$(`#PONumber`).val()}":{
      "dateOrder": "${$(`#docDate`).val()}",
        "dateDelivery": "${$(`#deliveryDate`).val()}",
        "proponent": "${proponent}",
        "supplier": {
          "contact": "${$(`#contactPerson`).val()}",
          "tel": "${$(`#tel`).val()}",
          "fax": "${$(`#fax`).val()}",
          "email": "${$(`#email`).val()}",
          "address": "${$(`#address`).val().replace(/\r?\n/g, '<br>')}",
          "remarks": "${$(`#remarksContact`).val().replace(/\r?\n/g, '<br>')}"
        },
        "product": {
          "list": ${data},
          "subtotal": "${$(`#subtotal`).val()}",
          "paymentMethod": "${$(`#payment`).val()}",
          "discount": "${$(`#discount`).val()}",
          "tax": {`
    if ($(`#vat`).val() === `novat`) {
      allDataObj += `"type": "novat",
                    "value": "0.00"`
    } else if ($(`#vat`).val() === `excluded`) {
      allDataObj += `"type": "excluded",
                    "value": "${$(`#excluded`).attr(`data-exvat`)}"`
    } else if ($(`#vat`).val() === `included`) {
      allDataObj += `"type": "included",
                    "value": "${$(`#included`).attr(`data-invat`)}"`
    }
    allDataObj += `},
          "paymentTerm": "${$(`#paymentTerms`).val().replace(/\r?\n/g, '<br>')}",
          "remarks": "${$(`#remarks`).val().replace(/\r?\n/g, '<br>')}"
        },
        "total": "${$(`#total`).val()}",
        "status": "${$(`#status`).val()}",
        "statusRemarks": "${$(`#statusRemarks`).val().replace(/\r?\n/g, '<br>')}"
    }}`
    firebase.database().ref(`stores/${store}`).child(`bills`).set(JSON.parse(allDataObj))
    firebase.database().ref(`stores/${store}`).child(`billCount`).set($(`#PONumber`).val())
  })
  $(`#savedModal`).modal({
    backdrop: 'static',
    keyboard: false
  })
  
  if ($(`#status`).val() == `complete`) {
    firebase.database().ref(`stores/${store}/cost/sold/${d.getFullYear()}-${d.getMonth()}`).child($(`#PONumber`).val()).set({
      date : `${d.getDate()}-${d.getMonth()}-${d.getFullYear()}`,
      value : parseFloat($(`#total`).val().replace(/,/g, ''))
    })
  }
})

const toRequest = (position) => {
  if (position === `creator`) {
    window.location.href = `./ingredient-assessment.html?store=${store}&role=${role}`
  } else if (position === `viewer`) {
    window.location.href = `./ingredient-request.html?store=${store}&role=${role}`
  }
}