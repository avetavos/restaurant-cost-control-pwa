var storeName
var store = new URL(window.location.href).searchParams.get('store')
var user

$(document).ready(() => {
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(window).height() - 56)
    $('.row-menu').css('mex-height', $(window).height() - 56)
  }
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
    storeName = snapshot.val()
  })

  firebase.database().ref(`stores/${store}`).on(`value`, snapshot => {
    $(`#txt-store`).html(snapshot.val()[`storeName`])
  })

  rePage()
})

$('#delConfirm').keyup(() => {
  if ($('#delConfirm').val() !== storeName) {
    $('#delConfirm-btn').prop('disabled', true)
  } else {
    $('#delConfirm-btn').prop('disabled', false)
  }
})

$('#delConfirm-btn').click(() => {
  firebase.database().ref('stores/' + store).remove().then(() => {
    window.location.href = './store-list.html'
  }).catch((error) => {
    console.log(error)
  })
})

const offlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                        <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                      </a>
                    </li>
                    <li class="other">
                      <a role="button" href="#" class="text-muted">
                        <i class="material-icons">delete</i> ลบร้านค้า
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        window.location.href = './update-sales.html?store=' + store + '&role=manager'
      } else if (role === 'staffer') {
        window.location.href = './update-sales.html?store=' + store + '&role=staffer'
      }
    } else {
      window.location.href = './index.html'
    }
  })
}

const onlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
    var store = url.searchParams.get('store')
    var role = url.searchParams.get('role')
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        if (role === 'owner') {
          localforage.getItem('storesOwner', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="other">
                          <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                            <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./update-sales.html?store=${store}&role=owner">
                            <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-request.html?store=${store}&role=owner">
                            <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-list.html?store=${store}&role=owner">
                            <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./expenses.html?store=${store}&role=owner">
                            <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                          </a>
                        </li>
                        <li class="menu_active">
                          <a rel="noopener" href="./edit-access.html?store=${store}&role=owner">
                            <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./create-menulist.html?store=${store}&role=owner">
                            <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./edit-store.html?store=${store}&role=owner">
                            <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                          </a>
                        </li>
                        <li class="del_store">
                          <a role="button" href="#" data-toggle="modal" data-target="#delStore">
                            <i class="material-icons">delete</i> ลบร้านค้า
                          </a>
                        </li>
                        <li class="back_page">
                          <a role="button" href="./store-list.html">
                            <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                          </a>
                        </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        } else if (role === 'manager') {
          window.location.href = './update-sales.html?store=' + store + '&role=manager'
        } else if (role === 'staffer') {
          window.location.href = './update-sales.html?store=' + store + '&role=staffer'
        }
        firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
          $('#delStoreName').text(snapshot.val())
        })
      } else {
        window.location.href = './index.html'
      }
    })
}

window.addEventListener('online', () => {
  onlineWorker()
})

window.addEventListener('offline', () => {
  offlineWorker()
})

window.onresize = (event) => {
  $('.row-menu').css('height', $(window).height() - 56)
}

const accessModal = obj => {
  $(`#store-key`).val(store)
  $(`#user-key`).val(obj.attr(`data-target`))
  $(`#txt-name`).html(obj.attr(`data-name`))
  $(`#txt-tel`).html(obj.attr(`data-phone`))
  $('#tel-user').attr('href', (`tel:${obj.attr(`data-phone`)}`))
  $(`#txt-email`).html(obj.attr(`data-email`))
  $('#email-user').attr('href', (`mailto:${obj.attr(`data-email`)}`))
  $(`#access-level`).val(obj.attr(`data-role`))
  
  $(`#accessModal`).modal()
}

$(`#access-setting`).click(()=>{
  if ($(`#access-level`).val() === `0`) {
    firebase.database().ref(`stores/${$(`#store-key`).val()}/userList/${$(`#user-key`).val()}`).remove()
    rePage()
  }
  else {
    firebase.database().ref(`stores/${$(`#store-key`).val()}/userList`).child($(`#user-key`).val()).set($(`#access-level`).val())
    rePage()
  }
})

const rePage = () => {
  $(`#userList`).empty()

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser.uid
      firebase.database().ref(`stores/${store}/userList`).orderByValue().equalTo(`owner`).on(`value`, function(snapshot) {
        for (var userKey in snapshot.val()) {
          var ownerTxt = ``
          firebase.database().ref(`users/${userKey}`).on(`value`, function(userSnapshot) {
            if (user == userKey) {
              ownerTxt += `<li data-target="${userKey}" data-role="owner" data-email="${userSnapshot.val()[`email`]}" data-phone="${userSnapshot.val()[`phone`]}" data-name="${userSnapshot.val()[`fullname`]}"><img src="./assets/images/owner.png" alt="owner"> <p>${userSnapshot.val()[`fullname`]}<br><span class="text-muted">เจ้าของกิจการ</span></p></li>`
            } else {
              ownerTxt += `<li data-target="${userKey}" data-role="owner" data-email="${userSnapshot.val()[`email`]}" data-phone="${userSnapshot.val()[`phone`]}" data-name="${userSnapshot.val()[`fullname`]}" onclick="accessModal($(this))"><img src="./assets/images/owner.png" alt="owner"> <p>${userSnapshot.val()[`fullname`]}<br><span class="text-muted">เจ้าของกิจการ</span></p></li>`
            }
            $(`#userList`).append(ownerTxt)
          })
        }
      })
    
      firebase.database().ref(`stores/${store}/userList`).orderByValue().equalTo(`manager`).on(`value`, function(snapshot) {
        for (var userKey in snapshot.val()) {
          var managerTxt = ``
          firebase.database().ref(`users/${userKey}`).on(`value`, function(userSnapshot) {
            managerTxt += `<li data-target="${userKey}" data-role="manager" data-email="${userSnapshot.val()[`email`]}" data-phone="${userSnapshot.val()[`phone`]}" data-name="${userSnapshot.val()[`fullname`]}" onclick="accessModal($(this))"><img src="./assets/images/manager.png" alt="manager"> <p>${userSnapshot.val()[`fullname`]}<br><span class="text-muted">ผู้จัดการร้าน</span></p></li>`
            $(`#userList`).append(managerTxt)
          })
        }
      })
    
      firebase.database().ref(`stores/${store}/userList`).orderByValue().equalTo(`staffer`).on(`value`, function(snapshot) {
        for (var userKey in snapshot.val()) {
          var stafferTxt = ``
          firebase.database().ref(`users/${userKey}`).on(`value`, function(userSnapshot) {
            stafferTxt += ` <li data-target="${userKey}" data-role="staffer" data-email="${userSnapshot.val()[`email`]}" data-phone="${userSnapshot.val()[`phone`]}" data-name="${userSnapshot.val()[`fullname`]}" onclick="accessModal($(this))"><img src="./assets/images/staffer.png" alt="staffer"> <p>${userSnapshot.val()[`fullname`]}<br><span class="text-muted">พนักงานทั่วไป</span></p></li>`
            $(`#userList`).append(stafferTxt)
          })
        }
      })
    }
  })
}