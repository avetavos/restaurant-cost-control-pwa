var storeName
var url = new URL(window.location.href)
var store = url.searchParams.get('store')
var role = url.searchParams.get('role')
var user = firebase.auth().currentUser

$(document).ready(() => {
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(window).height() - 56)
    $('.row-menu').css('mex-height', $(window).height() - 56)
  }
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser
      firebase.database().ref(`stores/${store}/bills`).orderByChild(`proponent`).equalTo(user.uid).on(`value`, snapshot => {
        for (var key in snapshot.val()) {
          if (snapshot.val()[key] !== null) {
            $(`#po-list tbody`).empty()
            $(`#po-empty`).addClass(`d-none`)
            var paymentMethod = ``
            if (snapshot.val()[key][`product`][`paymentMethod`] === `cash`) {
              paymentMethod += `เงินสด`
            } else if (snapshot.val()[key][`product`][`paymentMethod`] === `15 day credit`) {
              paymentMethod += `เครดิต 15 วัน`
            } else if (snapshot.val()[key][`product`][`paymentMethod`] === `30 day credit`) {
              paymentMethod += `เครดิต 30 วัน`
            } else if (snapshot.val()[key][`product`][`paymentMethod`] === `45 day credit`) {
              paymentMethod += `เครดิต 45 วัน`
            } else if (snapshot.val()[key][`product`][`paymentMethod`] === `60 day credit`) {
              paymentMethod += `เครดิต 60 วัน`
            } else if (snapshot.val()[key][`product`][`paymentMethod`] === `bill payment`) {
              paymentMethod += `ชำระเงินผ่านบิลค์`
            }
            var createList = ``
            if (snapshot.val()[key][`status`] === `complete`) {
              createList += `<tr class="bg-complete" onclick="window.location.href='./ingredient-edit.html?store=${store}&role=${role}&po=${key}'">
                              <td>${key}</td>
                              <td>${snapshot.val()[key][`dateOrder`].split(`-`)[2]}/${snapshot.val()[key][`dateOrder`].split(`-`)[1]}/${snapshot.val()[key][`dateOrder`].split(`-`)[0]}</td>
                              <td>อนุมัติ</td>`
                              if (snapshot.val()[key][`statusRemarks`] != null) {
                                createList += `<td>${snapshot.val()[key][`statusRemarks`]}</td></tr>`
                              } else {
                                createList += `<td></td></tr>`
                              }
            } else if (snapshot.val()[key][`status`] === `awaiting`) {
              createList += `<tr class="bg-awaiting" onclick="window.location.href='./ingredient-edit.html?store=${store}&role=${role}&po=${key}'">
                              <td>${key}</td>
                              <td>${snapshot.val()[key][`dateOrder`].split(`-`)[2]}/${snapshot.val()[key][`dateOrder`].split(`-`)[1]}/${snapshot.val()[key][`dateOrder`].split(`-`)[0]}</td>
                              <td>รอการประเมิน</td>`
                              if (snapshot.val()[key][`statusRemarks`] != null) {
                                createList += `<td>${snapshot.val()[key][`statusRemarks`]}</td></tr>`
                              } else {
                                createList += `<td></td></tr>`
                              }
            } else if (snapshot.val()[key][`status`] === `fail`) {
              createList += `<tr class="bg-fail" onclick="window.location.href='./ingredient-edit.html?store=${store}&role=${role}&po=${key}'">
                              <td>${key}</td>
                              <td>${snapshot.val()[key][`dateOrder`].split(`-`)[2]}/${snapshot.val()[key][`dateOrder`].split(`-`)[1]}/${snapshot.val()[key][`dateOrder`].split(`-`)[0]}</td>
                              <td>ไม่อนุมัติ</td>`
                              if (snapshot.val()[key][`statusRemarks`] != null) {
                                createList += `<td>${snapshot.val()[key][`statusRemarks`]}</td></tr>`
                              } else {
                                createList += `<td></td></tr>`
                              }
                            
            }
            $(`#po-list tbody`).append(createList)
            $(`#help-table`).removeClass(`d-none`)
          }
        }
      })
    }
  })
})

$('#delConfirm').keyup(() => {
  if ($('#delConfirm').val() !== storeName) {
    $('#delConfirm-btn').prop('disabled', true)
  }
  else {
    $('#delConfirm-btn').prop('disabled', false)
  }
})

$('#delConfirm-btn').click(()=>{
  firebase.database().ref('stores/' + store).remove().then(()=>{
    window.location.href = './store-list.html'
  }).catch((error)=>{
    console.log(error)
  })
})

window.onresize = (event) => {
  $('.row-menu').css('height', $(window).height() - 56)
}

const onlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
    var store = url.searchParams.get('store')
    var role = url.searchParams.get('role')
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        if (role === 'owner') {
          localforage.getItem('storesOwner', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="other">
                        <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                          <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=owner">
                          <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-request.html?store=${store}&role=owner">
                          <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=owner">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./expenses.html?store=${store}&role=owner">
                          <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./edit-access.html?store=${store}&role=owner">
                          <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./create-menulist.html?store=${store}&role=owner">
                          <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./edit-store.html?store=${store}&role=owner">
                          <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                        </a>
                      </li>
                      <li class="del_store">
                        <a role="button" href="#" data-toggle="modal" data-target="#delStore">
                          <i class="material-icons">delete</i> ลบร้านค้า
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        } else if (role === 'manager') {
          localforage.getItem('storesManager', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="other">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=manager">
                          <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-request.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=manager">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="menu_active">
                        <a rel="noopener" href="./ingredient-assessment.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./expenses.html?store=${store}&role=manager">
                          <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        } else if (role === 'staffer') {
          localforage.getItem('storesStaffer', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="other">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=staffer">
                          <i class="material-icons">assignment_late</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=staffer">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="menu_active">
                        <a rel="noopener" href="./ingredient-assessment.html?store=${store}&role=staffer">
                          <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        }
        firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
          $('#delStoreName').text(snapshot.val())
        })
      } else {
        window.location.href = './index.html'
      }
    })
}

const offlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                        <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                      </a>
                    </li>
                    <li class="other">
                      <a role="button" href="#" class="text-muted">
                        <i class="material-icons">delete</i> ลบร้านค้า
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        localforage.getItem('storesManager', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'staffer') {
        localforage.getItem('storesStaffer', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      }
    } else {
      window.location.href = './index.html'
    }
  })
}

window.addEventListener('online', () => {
  onlineWorker()
})

window.addEventListener('offline', () => {
  offlineWorker()
})