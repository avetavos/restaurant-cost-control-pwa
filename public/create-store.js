var storeCode = makeStoreID()
$(document).ready(() => {
  var refCheck = firebase.database().ref("stores");
  refCheck.orderByChild("storeCode").equalTo(storeCode).on("child_added", function (snapshot) {
    if (snapshot.key !== '') {
      storeCode = makeStoreID()
    }
  });
})

$('#create-store').click(() => {
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser;
      if (user) {
        let firebaseRef = firebase.database().ref('stores')
        firebaseRef.push({
          userList: {
            [user.uid]: 'owner'
          },
          storeName: $('#storename').val(),
          storeType: $('#storetype').val(),
          storeCode: storeCode,
          storeAdd: $('#storeaddress').val().replace(/\r?\n/g, '<br>')
        }).then(() => {
          var storeRef = firebase.database().ref('stores')
          storeRef.orderByChild('userList/' + user.uid).equalTo('owner').on('value',(snapshot)=>{
            localforage.setItem('storesOwner', snapshot.val()).then(()=>{
              window.location.href = './store-list.html'
            })
          })
        })
      }
    }
    else {
      window.location.href = `./index.html`
    }
  })
})

function makeStoreID() {
  let text = "";
  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 4; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

window.addEventListener('online',  ()=>{
  $('#create-store').attr('disabled', false)
})

window.addEventListener('offline', ()=>{
  $('#create-store').attr('disabled', true)
})