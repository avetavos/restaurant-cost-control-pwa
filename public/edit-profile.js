$(document).ready(() => {
  if (navigator.onLine) {

  } else {
    window.location.href = './index.html'
  }
  
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(document).height() - 56)
  }
  
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser
      localforage.getItem('users', (err, userData) => {
        $('#show_name').text(userData[String(user.uid)]['fullname'])
        $('#show_name_md').text(userData[String(user.uid)]['fullname'])
        $('#username').val(userData[String(user.uid)]['email'])
        $('#fullName').val(userData[String(user.uid)]['fullname'])
        $('#telNumber').val(userData[String(user.uid)]['phone'])
      })
      firebase.database().ref(`stores`).orderByChild(`userList/${user.uid}`).equalTo(`owner`).on(`value`, snapshot => {
        if (snapshot.val() != null) {
          for (var key in snapshot.val()) {
            firebase.database().ref(`stores/${key}/accessRequest`).on(`value`, snapshot => {
              if (snapshot.val() != null) {
                $(`#notificationCount`).html(snapshot.numChildren())
                $(`#notificationCount-md`).html(snapshot.numChildren())
                $('#access-alert').addClass('text-danger font-weight-bold')
                $('#access-alert-md').addClass('text-danger font-weight-bold')
              }
            })
          }
        }
      })
    } else {
      window.location.href = `./index.html`
    }
  })
})

$('#update-btn').click(() => {
  $('#show_name').text($('#fullName').val())
  $('#show_name_md').text($('#fullName').val())
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser;
      if (user != null) {
        email = user.email
        var ref = firebase.database().ref("users/" + user.uid)
        if ($('#password').val() === '') {
          ref.update({
            fullname: $('#fullName').val(),
            phone: $('#telNumber').val()
          }).then(() => {
            $('#save-modal').modal('show')
          })
        } 
        else {
          var newPassword = $('#password').val()
          user.updatePassword(newPassword)
          ref.update({
            fullname: $('#fullName').val(),
            phone: $('#telNumber').val(),
          }).then(() => {
            $('#save-modal').modal('show')
          })
        }
        ref.on('value',snapshot=>{
          var userData =  '{"' + snapshot.key + '":'+ JSON.stringify(snapshot.val()) + '}'
          console.log(userData)
          localforage.setItem('users', JSON.parse(userData))
        })
      }
    } else {
      window.location.href = `./index.html`
    }
  })
})

$('#password').keyup(() => {
  if (($('#password').val().length == 0 && $('#conPassword').val().length == 0) || ($('#password').val().length < 8 && $('#conPassword').val().length < 8) || ($('#password').val() !== $('#conPassword').val())) {
    $('#update-btn').prop('disabled', true)
  } else {
    $('#update-btn').prop('disabled', false)
  }
})

$('#conPassword').keyup(() => {
  if (($('#password').val().length == 0 && $('#conPassword').val().length == 0) || ($('#password').val().length < 8 && $('#conPassword').val().length < 8) || ($('#password').val() !== $('#conPassword').val())) {
    $('#update-btn').prop('disabled', true)
  } else {
    $('#update-btn').prop('disabled', false)
  }
})

$('form').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

window.addEventListener('online',  ()=>{
  $('#update-btn').attr('disabled', false)
  $('#profile-link-md').attr('href', './edit-profile.html')
  $('#profile-link-md').removeClass('text-muted')
  $('#search-link-md').attr('href', './search-store.html')
  $('#search-link-md').removeClass('text-muted')
  $('#access-alert-md').attr('href', './access-request.html')
  $('#access-alert-md').removeClass('text-muted')
  $('#profile-link').attr('href', './edit-profile.html')
  $('#profile-link').removeClass('text-muted')
  $('#search-link').attr('href', './search-store.html')
  $('#search-link').removeClass('text-muted')
  $('#access-alert').attr('href', './access-request.html')
  $('#access-alert').removeClass('text-muted')
})

window.addEventListener('offline', ()=>{
  $('#update-btn').attr('disabled', true)
  $('#profile-link-md').removeAttr('href')
  $('#profile-link-md').addClass('text-muted')
  $('#search-link-md').removeAttr('href')
  $('#search-link-md').addClass('text-muted')
  $('#access-alert-md').removeAttr('href')
  $('#access-alert-md').addClass('text-muted')
  $('#profile-link').removeAttr('href')
  $('#profile-link').addClass('text-muted')
  $('#search-link').removeAttr('href')
  $('#search-link').addClass('text-muted')
  $('#access-alert').removeAttr('href')
  $('#access-alert').addClass('text-muted')
})

window.onresize = (event)=>{
  $('.row-menu').css('height', $(window).height() - 56)
}