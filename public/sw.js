self.addEventListener('install', e => {
  const timeStamp = Date.now();
  e.waitUntil(
    caches.open('restaurantCostControlData').then(cache => {
      return cache.addAll([
          `/`,
          `./node_modules/material-design-icons/iconfont/material-icons.css?timestamp=${timeStamp}`,
          `./node_modules/material-design-icons/iconfont/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2?timestamp=${timeStamp}`,
          `./node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css?timestamp=${timeStamp}`,
          `./node_modules/bootstrap-material-design/dist/js/bootstrap-material-design.min.js?timestamp=${timeStamp}`,
          `./node_modules/jquery/dist/jquery.slim.min.js?timestamp=${timeStamp}`,
          `./node_modules/popper.js/dist/umd/popper-utils.min.js?timestamp=${timeStamp}`,
          `./node_modules/chart.js/dist/Chart.min.js?timestamp=${timeStamp}`,
          `./node_modules/localforage/dist/localforage.min.js?timestamp=${timeStamp}`,
          `./node_modules/table-to-json/src/jquery.tabletojson.js?timestamp=${timeStamp}`,
          `./assets/fonts/SukhumvitSet-SemiBold.ttf?timestamp=${timeStamp}`,
          `./assets/icons/icon_512.png?timestamp=${timeStamp}`,
          `./assets/icons/icon_128.png?timestamp=${timeStamp}`,
          `./assets/icons/icon_144.png?timestamp=${timeStamp}`,
          `./assets/icons/icon_152.png?timestamp=${timeStamp}`,
          `./assets/icons/icon_16.png?timestamp=${timeStamp}`,
          `./assets/icons/icon_192.png?timestamp=${timeStamp}`,
          `./assets/icons/icon_256.png?timestamp=${timeStamp}`,
          `./assets/icons/icon_32.png?timestamp=${timeStamp}`,
          `./css/access-request.css?timestamp=${timeStamp}`,
          `./css/create-store.css?timestamp=${timeStamp}`,
          `./css/edit-profile.css?timestamp=${timeStamp}`,
          `./css/index.css?timestamp=${timeStamp}`,
          `./css/menus.css?timestamp=${timeStamp}`,
          `./css/register.css?timestamp=${timeStamp}`,
          `./css/search-store.css?timestamp=${timeStamp}`,
          `./css/store-list.css?timestamp=${timeStamp}`,
          `./css/store-menu.css?timestamp=${timeStamp}`,
          `./js/chart-data.js?timestamp=${timeStamp}`,
          `./js/find-in-json.js?timestamp=${timeStamp}`,
          `./js/get-month-year.js?timestamp=${timeStamp}`,
          `./js/connection.js?timestamp=${timeStamp}`,
          `./js/list-menus.js?timestamp=${timeStamp}`,
          `./index.html?timestamp=${timeStamp}`,
          `./index.js?timestamp=${timeStamp}`,
          `./sign-in.html?timestamp=${timeStamp}`,
          `./sign-in.js?timestamp=${timeStamp}`,
          `./register.html?timestamp=${timeStamp}`,
          `./register.js?timestamp=${timeStamp}`,
          `./store-list.html?timestamp=${timeStamp}`,
          `./store-list.js?timestamp=${timeStamp}`,
          `./edit-profile.html?timestamp=${timeStamp}`,
          `./edit-profile.js?timestamp=${timeStamp}`,
          `./search-store.html?timestamp=${timeStamp}`,
          `./search-store.js?timestamp=${timeStamp}`,
          `./access-request.html?timestamp=${timeStamp}`,
          `./access-request.js?timestamp=${timeStamp}`,
          `./store-menu.html?timestamp=${timeStamp}`,
          `./store-menu.js?timestamp=${timeStamp}`,
          `./css/store-menu.css?timestamp=${timeStamp}`,
          `./update-sales.html?timestamp=${timeStamp}`,
          `./update-sales.js?timestamp=${timeStamp}`,
          `./css/update-sales.css?timestamp=${timeStamp}`,
          `./ingredient-request.html?timestamp=${timeStamp}`,
          `./ingredient-request.js?timestamp=${timeStamp}`,
          `./css/ingredient-request.css?timestamp=${timeStamp}`,
          `./ingredient-list.html?timestamp=${timeStamp}`,
          `./ingredient-list.js?timestamp=${timeStamp}`,
          `./css/ingredient-list.css?timestamp=${timeStamp}`,
          `./ingredient-edit.html?timestamp=${timeStamp}`,
          `./ingredient-edit.js?timestamp=${timeStamp}`,
          `./css/ingredient-edit.css?timestamp=${timeStamp}`,
          `./ingredient-assessment.html?timestamp=${timeStamp}`,
          `./ingredient-assessment.js?timestamp=${timeStamp}`,
          `./css/ingredient-assessment.css?timestamp=${timeStamp}`,
          `./expenses.html?timestamp=${timeStamp}`,
          `./expenses.js?timestamp=${timeStamp}`,
          `./css/expenses.css?timestamp=${timeStamp}`,
          `./electricity.html?timestamp=${timeStamp}`,
          `./electricity.js?timestamp=${timeStamp}`,
          `./water.html?timestamp=${timeStamp}`,
          `./water.js?timestamp=${timeStamp}`,
          `./rent.html?timestamp=${timeStamp}`,
          `./rent.js?timestamp=${timeStamp}`,
          `./salary.html?timestamp=${timeStamp}`,
          `./salary.js?timestamp=${timeStamp}`,
          `./other-expenses.html?timestamp=${timeStamp}`,
          `./other-expenses.js?timestamp=${timeStamp}`,
          `./edit-access.html?timestamp=${timeStamp}`,
          `./edit-access.js?timestamp=${timeStamp}`,
          `./css/edit-access.css?timestamp=${timeStamp}`,
          `./create-menulist.html?timestamp=${timeStamp}`,
          `./create-menulist.js?timestamp=${timeStamp}`,
          `./css/create-menulist.css?timestamp=${timeStamp}`,
          `./edit-store.html?timestamp=${timeStamp}`,
          `./edit-store.js?timestamp=${timeStamp}`,
          `./css/edit-store.css?timestamp=${timeStamp}`
        ])
        .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request, {
      ignoreSearch: true
    }).then(response => {
      return response || fetch(event.request);
    })
  );
});