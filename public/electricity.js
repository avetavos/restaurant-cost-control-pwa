var storeName
var store = new URL(window.location.href).searchParams.get('store')
var role = new URL(window.location.href).searchParams.get('role')
var d = new Date()

$(document).ready(() => {
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(window).height() - 56)
    $('.row-menu').css('mex-height', $(window).height() - 56)
  }
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
    storeName = snapshot.val()
  })

  $(`#electricityNow`).html(`ประจำเดือน ${monthNames[d.getMonth()]} ปี ${thaiYear(d.getFullYear())}`)

  checkCost()
})

$('#delConfirm').keyup(() => {
  if ($('#delConfirm').val() !== storeName) {
    $('#delConfirm-btn').prop('disabled', true)
  } else {
    $('#delConfirm-btn').prop('disabled', false)
  }
})

$('#delConfirm-btn').click(() => {
  firebase.database().ref('stores/' + store).remove().then(() => {
    window.location.href = './store-list.html'
  }).catch((error) => {
    console.log(error)
  })
})

window.onresize = (event) => {
  $('.row-menu').css('height', $(window).height() - 56)
}

const offlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                        <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                      </a>
                    </li>
                    <li class="other">
                      <a role="button" href="#" class="text-muted">
                        <i class="material-icons">delete</i> ลบร้านค้า
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        localforage.getItem('storesManager', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'staffer') {
        window.location.href = `./update-sales.html?store=${store}&role=staffer`
      }
    } else {
      window.location.href = './index.html'
    }
  })
}

const onlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                          <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                            <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./update-sales.html?store=${store}&role=owner">
                            <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-request.html?store=${store}&role=owner">
                            <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-list.html?store=${store}&role=owner">
                            <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                          </a>
                        </li>
                        <li class="menu_active">
                          <a rel="noopener" href="./expenses.html?store=${store}&role=owner">
                            <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./edit-access.html?store=${store}&role=owner">
                            <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./create-menulist.html?store=${store}&role=owner">
                            <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./edit-store.html?store=${store}&role=owner">
                            <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                          </a>
                        </li>
                        <li class="del_store">
                          <a role="button" href="#" data-toggle="modal" data-target="#delStore">
                            <i class="material-icons">delete</i> ลบร้านค้า
                          </a>
                        </li>
                        <li class="back_page">
                          <a role="button" href="./store-list.html">
                            <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                          </a>
                        </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        localforage.getItem('storesManager', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=manager">
                          <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-request.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=manager">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-assessment.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="menu_active">
                        <a rel="noopener" href="./expenses.html?store=${store}&role=manager">
                          <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'staffer') {
        window.location.href = `./update-sales.html?store=${store}&role=staffer`
      }
      firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
        $('#delStoreName').text(snapshot.val())
      })
    } else {
      window.location.href = './index.html'
    }
  })
}

window.addEventListener('online', () => {
  onlineWorker()
})

window.addEventListener('offline', () => {
  offlineWorker()
})

const checkCost = () => {
  firebase.database().ref(`stores/${store}/cost/electricity`).orderByChild(`${d.getFullYear()}-${d.getMonth()}`).on(`value`, snapshot => {
    if (snapshot.numChildren() == 0) {
      $(`#inputElectricity`).removeClass(`d-none`)
      $(`#disabledElectricity`).addClass(`d-none`)
    } else {
      $(`#inputElectricity`).addClass(`d-none`)
      $(`#disabledElectricity`).removeClass(`d-none`)
      for (var date in snapshot.val()) {
        var arrDate = (snapshot.val()[date][`date`]).split(`-`)
        $(`#saveDate`).html(`${arrDate[0]} ${monthNames[arrDate[1]]} ${thaiYear(parseInt(arrDate[2]))}`)
        $(`#saveVal`).html(formatMoney(parseFloat(snapshot.val()[date][`value`])))
        firebase.database().ref(`users/${snapshot.val()[date][`user`]}`).on(`value`, snapshot => {
          $(`#saveUser`).html(snapshot.val()[`fullname`])
        })
      }
    }
  })
}

$(`#save-electricity`).click(()=>{
  $(`#confirmMonth`).html(monthNames[d.getMonth()])
  $(`#confirmYear`).html(thaiYear(d.getFullYear()))
  $(`#confirmValue`).html(formatMoney(parseFloat($(`#electricityValue`).val())))
  $(`#savedConfirmModal`).modal()
})

$(`#confirmSave`).click(() => {
  $(`#savedConfirmModal`).modal(`hide`)
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser
      firebase.database().ref(`stores/${store}/cost/electricity`).child(`${d.getFullYear()}-${d.getMonth()}`).set({
        date : `${d.getDate()}-${d.getMonth()}-${d.getFullYear()}`,
        user : user.uid,
        value : $(`#electricityValue`).val()
      })
      checkCost()
    }
  })
  $(`#savedModal`).modal()
})

const formatMoney = (num) => {
  if (num !== ``) {
    var p = num.toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
  } else {
    return `0.00`
  }
}

const inputNumber = (e) => {
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
  (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode >= 35 && e.keyCode <= 39)) {
      return;
  }
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
}

$(`#get-save`).click(()=>{
  window.location.href = `./expenses.html?store=${store}&role=${role}`
})

$(`#edit-save`).click(()=>{
  firebase.database().ref(`stores/${store}/cost/electricity/${d.getFullYear()}-${d.getMonth()}`).remove()
})

$('#electricityValue').keyup(function() {
  if ($(this).val() != null && $(this).val() != ``) {
    $(`#save-electricity`).prop(`disabled`, false)
  } else {
    $(`#save-electricity`).prop(`disabled`, true)
  }
})

$("#electricityValue").keyup(function(event) {
  if (event.keyCode === 13) {
    $("#save-electricity").click();
  }
})