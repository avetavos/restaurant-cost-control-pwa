var key
var uid
var store
$(document).ready(() => {
  if($(document).width() >= 768) {
    $('.row-menu').css('height', $(document).height() - 56)
  }

  if (navigator.onLine) {

  } else {
    window.location.href = './index.html'
  }
  
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser;
      if (user != null) {
        var user = firebase.auth().currentUser
        localforage.getItem('users',(err, userData)=>{
          $('#show_name').text(userData[String(user.uid)]['fullname'])
          $('#show_name_md').text(userData[String(user.uid)]['fullname'])
        })
        firebase.database().ref(`stores`).orderByChild(`userList/${user.uid}`).equalTo(`owner`).on(`value`, snapshot => {
          if (snapshot.val() != null) {
            for (var key in snapshot.val()) {
              firebase.database().ref(`stores/${key}/accessRequest`).on(`value`, snapshot => {
                if (snapshot.val() != null) {
                  $(`#notificationCount`).html(snapshot.numChildren())
                  $(`#notificationCount-md`).html(snapshot.numChildren())
                  $('#access-alert').addClass('text-danger font-weight-bold')
                  $('#access-alert-md').addClass('text-danger font-weight-bold')
                }
              })
            }
          }
        })
      }
    } else {
      window.location.href = `./index.html`
    }
  })
})

$('#search-btn').click(() => {
  var searchRef = firebase.database().ref("stores");
  searchRef.orderByChild("storeCode").equalTo($('#storecode').val()).once("value", (snapshot)=>{
    $('#result-search').empty()
    var storeKey = ''
    for(storeKey in snapshot.val()) {
      storeKey = storeKey
    }
    if (storeKey != ``) {
      var targetRef = firebase.database().ref(`stores/${storeKey}`)
        targetRef.once('value').then((data) => {
          let txt = `<div class="col-12 store__name mb-3 py-2" onClick="requestAccess(this)" store=${storeKey}>
                      <div class="row m-0">
                        <div class="col-12 text-right p-0">รหัสร้าน
                          <span class="font-weight-bold display-5">` + data.val().storeCode + `</span>
                        </div>
                        <div class="col-12 text-center py-2">
                          <h2 class="m-0">` + data.val().storeName + `</h2>
                        </div>
                        <div class="col-12">
                          <p class="font-weight-bold m-0">ที่อยู่ร้าน</p>
                        </div>
                        <div class="col-12">
                          <p class="m-0">` + data.val().storeAdd + `</p>
                        </div>
                      </div>
                    </div>`
          $('#result-search').append(txt)
          $('#store-name').text(data.val().storeName)
          return key = `stores/${storeKey}`
      })
    }
    else {
      let txt = `<div class="col-12 text-center">
                  <h2>ไม่พบร้านที่มีรหัสตรงตามที่ระบุ</h2>
                </div>`
      $('#result-search').append(txt)
    }
    return store = storeKey
  })
})

const requestAccess = (obj) => {
  var uid = firebase.auth().currentUser.uid;
  var checkOwnRef = firebase.database().ref("stores/" + $(obj).attr('store') + "/userList/" + uid)
  checkOwnRef.once("value", (snapshot)=>{
    if (snapshot.val() === null) {
      $('#requestModal').modal('show')
    }
    else {
      $('#ownerModal').modal('show')
    }
  })
}

$("#storecode").keyup(function(event) {
  if (event.keyCode === 13) {
    $("#search-btn").click();
  }
});

$('#comfirm-request').click(()=>{
  $('#waitModal').modal('show')
  var uid = firebase.auth().currentUser.uid;
  var checkRef = firebase.database().ref("stores/" + store + "/accessRequest");
  checkRef.orderByChild("userID").equalTo(uid).once("value", snapshot=>{
    if (snapshot.numChildren() >= 1) {
      console.log('your already request')
    }
    else {
      firebase.database().ref(key + "/accessRequest").child(uid).set('request')
    }
  })
})

$('form').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

window.addEventListener('online',  ()=>{
  $('#search-btn').attr('disabled', false)
  $('#profile-link-md').attr('href', './edit-profile.html')
  $('#profile-link-md').removeClass('text-muted')
  $('#search-link-md').attr('href', './search-store.html')
  $('#search-link-md').removeClass('text-muted')
  $('#access-alert-md').attr('href', './access-request.html')
  $('#access-alert-md').removeClass('text-muted')
  $('#profile-link').attr('href', './edit-profile.html')
  $('#profile-link').removeClass('text-muted')
  $('#search-link').attr('href', './search-store.html')
  $('#search-link').removeClass('text-muted')
  $('#access-alert').attr('href', './access-request.html')
  $('#access-alert').removeClass('text-muted')
})

window.addEventListener('offline', ()=>{
  $('#search-btn').attr('disabled', true)
  $('#profile-link-md').removeAttr('href')
  $('#profile-link-md').addClass('text-muted')
  $('#search-link-md').removeAttr('href')
  $('#search-link-md').addClass('text-muted')
  $('#access-alert-md').removeAttr('href')
  $('#access-alert-md').addClass('text-muted')
  $('#profile-link').removeAttr('href')
  $('#profile-link').addClass('text-muted')
  $('#search-link').removeAttr('href')
  $('#search-link').addClass('text-muted')
  $('#access-alert').removeAttr('href')
  $('#access-alert').addClass('text-muted')
})

window.onresize = (event)=>{
  $('.row-menu').css('height', $(window).height() - 56)
}