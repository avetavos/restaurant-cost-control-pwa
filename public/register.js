$('#password').keyup(() => {
  if (($('#password').val().length == 0 && $('#conPassword').val().length == 0) || ($('#password').val().length < 8 && $('#conPassword').val().length < 8) || ($('#password').val() !== $('#conPassword').val())) {
    $('#createUser').prop('disabled', true)
  }
  else {
    $('#createUser').prop('disabled', false)
  }
})

$('#conPassword').keyup(() => {
  if (($('#password').val().length == 0 && $('#conPassword').val().length == 0) || ($('#password').val().length < 8 && $('#conPassword').val().length < 8) || ($('#password').val() !== $('#conPassword').val())) {
    $('#createUser').prop('disabled', true)
  }
  else {
    $('#createUser').prop('disabled', false)
  }
})

$('#createUser').click(() => {
  firebase.auth().createUserWithEmailAndPassword($('#username').val(), $('#password').val())
    .then(function (user) {
      let firebaseRef = firebase.database().ref("users")
      firebaseRef.child(user.uid).set({
        email: $('#username').val(),
        fullname: $('#fullname').val(),
        phone: $('#telNumber').val(),
      })
    })
    .catch(function (error) {
      let errorCode = error.code
      let errorMessage = error.message
      if (errorCode === 'auth/weak-password') {
        $('#weak_password_modal').modal('show')
      } else if (errorCode === 'auth/email-already-in-use') {
        $('#email_already_modal').modal('show')
      } else if (errorCode === 'auth/invalid-email') {
        $('#invalid_email_modal').modal('show')
      } else if (errorCode === 'auth/operation-not-allowed') {
        $('#invalid_email_modal').modal('show')
      }
    })
})

$('#reg-complete').click(()=>{
  firebase.auth().signOut().then(function() {
    window.location.href = './index.html'
  })
})

$("#telNumber").keydown((e)=>{
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode >= 35 && e.keyCode <= 39)) {
      return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});