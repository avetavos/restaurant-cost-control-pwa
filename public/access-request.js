$(document).ready(() => {
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(document).height() - 56)
  }
})

window.onresize = (event)=>{
  $('.row-menu').css('height', $(window).height() - 56)
}

const displayList = key => {
  firebase.database().ref('stores/' + key + '/accessRequest').on('value', snapshot => {
    if (snapshot.val() != null) {
      $(`#notificationCount`).html(snapshot.numChildren())
      $(`#notificationCount-md`).html(snapshot.numChildren())
      $('#null-list').remove()
      $('#access-alert').addClass('text-danger font-weight-bold')
      $('#access-alert-md').addClass('text-danger font-weight-bold')
      for (var user in snapshot.val()) {
        createAlert(key, user)
      }
    } else {
      $('#request-list').empty()
      $('#access-alert').removeClass('text-danger font-weight-bold')
      $('#access-alert-md').removeClass('text-danger font-weight-bold')
      $(`#notificationCount`).empty()
      $(`#notificationCount-md`).empty()
      var txt = `<div class="col-12 text-center p-5" id="null-list">
                  <h2>ไม่มีคำร้องขอสิทธิ์การเข้าถึงร้านใดในขณะนี้</h2>
                </div>`
      $('#request-list').append(txt)
    }
  })
}

const onlineWorker = () => {
  $('#offline-alert').remove()
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      var user = firebase.auth().currentUser;
      if (user != null) {
        localforage.getItem('users', (err, userData) => {
          $('#show_name').text(userData[String(user.uid)]['fullname'])
          $('#show_name_md').text(userData[String(user.uid)]['fullname'])
        })
        firebase.database().ref('stores').orderByChild('userList/' + user.uid).equalTo('owner').on('value', snapshot => {
          $('#request-list').empty()
          if (snapshot.val() != null) {
            for (var key in snapshot.val()) {
              displayList(key)
            }
          } else {
            $('#access-alert').removeClass('text-danger font-weight-bold')
            $('#access-alert-md').removeClass('text-danger font-weight-bold')
            $('#request-list').empty()
            var txt = `<div class="col-12 text-center p-5" id="null-list">
                        <h2>ไม่มีคำร้องขอสิทธิ์การเข้าถึงร้านใดในขณะนี้</h2>
                      </div>`
            $('#request-list').append(txt)
          }
        })
      }
    } else {
      window.location.href = `./index.html`
    }
  })
}

const offlineWorker = () => {
  $('#request-list').empty()
  var txt = `<div class="col-12 text-center p-5" id="offline-alert">
              <h2>ไม่สามารถใช้งานได้โปรดเชื่อมต่ออินเตอร์เน็ต</h2>
            </div>`
  $('#request-list').append(txt)
}

const createAlert = (key, uid) => {
  var storeName, userName
  firebase.database().ref('stores/' + key).orderByKey().equalTo("storeName").on('child_added', snapshot => {
    storeName = snapshot.val()
    firebase.database().ref('users/' + uid).orderByKey().equalTo("fullname").on('child_added', snapshot => { 
      userName = snapshot.val()
      var txt = `<div class="col-lg-8 mx-auto col-12 store__name mb-3 px-0" onclick="callModal(this)" id="${uid + key}" key="${key}" uid="${uid}">
                  <div class="row m-0 p-1 bg-danger">
                    <div class="col-12 text-center m-0">
                      <p class="m-0 text-light">ขอสิทธิ์การเข้าถึงข้อมูลร้าน</p>
                    </div>
                    <div class="col-lg-8 mx-auto col-12 text-center">
                      <p class="font-weight-bold m-0 text-light">${storeName}</p>
                    </div>
                  </div>
                  <div class="row m-0">
                    <div class="col-12 text-center p-2">
                      <h3 class="my-1">${userName}</h3>
                    </div>
                  </div>
                </div>`
      $('#request-list').append(txt)
      })
  })
}

const callModal = (obj) => {
  $('#txt-name').empty()
  $('#txt-tel').empty()
  $('#txt-email').empty()
  firebase.database().ref('users').orderByKey().equalTo($(obj).attr('uid')).on('child_added', (snapshot)=>{
    $('#txt-name').text(snapshot.val().fullname)
    $('#txt-tel').text(snapshot.val().phone)
    $('#txt-email').text(snapshot.val().email)
    $('#tel-user').attr('href', ('tel:' + snapshot.val().phone))
    $('#email-user').attr('href', ('mailto:' + snapshot.val().email))
  })
  firebase.database().ref('stores').orderByKey().equalTo($(obj).attr('key')).on('child_added', (snapshot)=>{
    $('#txt-store').text(snapshot.val().storeName)
  })
  $('#store-key').val($(obj).attr('key'))
  $('#user-key').val($(obj).attr('uid'))
  $('#accessModal').modal('show')
}

$('#access-setting').click(()=>{
  if ($('#access-level').val() == '0') {
    firebase.database().ref('stores/' + $('#store-key').val() + '/accessRequest/' + $('#user-key').val()).remove()
  }
  else {
    firebase.database().ref('stores/' + $('#store-key').val() + '/userList').child($('#user-key').val()).set($('#access-level').val())
    .then(()=>{
      firebase.database().ref('stores/' + $('#store-key').val() + '/accessRequest/' + $('#user-key').val()).remove()
    })
  }
})

window.addEventListener('online',  ()=>{
  onlineWorker()
  $('#profile-link-md').attr('href', './edit-profile.html')
  $('#profile-link-md').removeClass('text-muted')
  $('#search-link-md').attr('href', './search-store.html')
  $('#search-link-md').removeClass('text-muted')
  $('#access-alert-md').attr('href', './access-request.html')
  $('#access-alert-md').removeClass('text-muted')
  $('#profile-link').attr('href', './edit-profile.html')
  $('#profile-link').removeClass('text-muted')
  $('#search-link').attr('href', './search-store.html')
  $('#search-link').removeClass('text-muted')
  $('#access-alert').attr('href', './access-request.html')
  $('#access-alert').removeClass('text-muted')
})

window.addEventListener('offline', ()=>{
  offlineWorker()
  $('#profile-link-md').removeAttr('href')
  $('#profile-link-md').addClass('text-muted')
  $('#search-link-md').removeAttr('href')
  $('#search-link-md').addClass('text-muted')
  $('#access-alert-md').removeAttr('href')
  $('#access-alert-md').addClass('text-muted')
  $('#profile-link').removeAttr('href')
  $('#profile-link').addClass('text-muted')
  $('#search-link').removeAttr('href')
  $('#search-link').addClass('text-muted')
  $('#access-alert').removeAttr('href')
  $('#access-alert').addClass('text-muted')
})