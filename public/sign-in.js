$('#login').click(() => {
  checkLogin()
})

const checkLogin = () => {
  var email = $('#username').val()
  var password = $('#password').val()
  firebase.auth().signInWithEmailAndPassword(email, password)
    .then((user) => {
      window.location.href = './index.html'
    })
    .catch((error) => {
      var errorCode = error.code
      var errorMessage = error.message
      if (errorCode === 'auth/invalid-email') {
        $('#invalid_email_modal').modal('show');
      } else if (errorCode === 'auth/user-disabled') {
        $('#user_disabled_modal').modal('show')
      } else if (errorCode === 'auth/user-not-found') {
        $('#not_found_modal').modal('show')
      } else if (errorCode === 'auth/wrong-password') {
        $('#wrong_password_modal').modal('show')
      }
    })
}

$("#username").keyup(function (event) {
  if (event.keyCode === 13) {
    $("#login").click();
  }
});

$("#password").keyup(function (event) {
  if (event.keyCode === 13) {
    $("#login").click();
  }
});

$('form').on('keyup keypress', function (e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) {
    e.preventDefault();
    return false;
  }
})