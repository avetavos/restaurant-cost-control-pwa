var storeName
var store = new URL(window.location.href).searchParams.get('store')
var d = new Date()

$(document).ready(()=>{
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(window).height() - 56)
    $('.row-menu').css('mex-height', $(window).height() - 56)
  }
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
    storeName = snapshot.val()
  })

  firebase.database().ref(`stores/${store}/menuList`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      var i = 1
      for (var key in snapshot.val()) {
        var txt = `<tr id="${key}" onclick="updateSell($(this))" data-price="${snapshot.val()[key][`menuPrice`]}">
                    <td>${i}</td>
                    <td>${snapshot.val()[key][`menuName`]}</td>
                    <td>0</td>
                  </tr>`
        i++
        getSalePrice(key)
        $(`#product-list tbody`).append(txt)
      }
      $(`#menu-empty`).addClass(`d-none`)
      $(`#help-table`).removeClass(`d-none`)
    } else {
      $(`#menu-empty`).removeClass(`d-none`)
      $(`#help-table`).addClass(`d-none`)
    }
  })
})

$('#delConfirm').keyup(() => {
  if ($('#delConfirm').val() !== storeName) {
    $('#delConfirm-btn').prop('disabled', true)
  }
  else {
    $('#delConfirm-btn').prop('disabled', false)
  }
})

$('#delConfirm-btn').click(()=>{
  firebase.database().ref('stores/' + store).remove().then(()=>{
    window.location.href = './store-list.html'
  }).catch((error)=>{
    console.log(error)
  })
})

window.onresize = (event)=>{
  $('.row-menu').css('height', $(window).height() - 56)
}

const onlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
    var store = url.searchParams.get('store')
    var role = url.searchParams.get('role')
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        if (role === 'owner') {
          localforage.getItem('storesOwner', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="other">
                        <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                          <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                        </a>
                      </li>
                      <li class="menu_active">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=owner">
                          <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-request.html?store=${store}&role=owner">
                          <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=owner">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./expenses.html?store=${store}&role=owner">
                          <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./edit-access.html?store=${store}&role=owner">
                          <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./create-menulist.html?store=${store}&role=owner">
                          <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./edit-store.html?store=${store}&role=owner">
                          <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                        </a>
                      </li>
                      <li class="del_store">
                        <a role="button" href="#" data-toggle="modal" data-target="#delStore">
                          <i class="material-icons">delete</i> ลบร้านค้า
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        } else if (role === 'manager') {
          localforage.getItem('storesManager', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="menu_active">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=manager">
                          <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-request.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=manager">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-assessment.html?store=${store}&role=manager">
                          <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./expenses.html?store=${store}&role=manager">
                          <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        } else if (role === 'staffer') {
          localforage.getItem('storesStaffer', (err, storeData) => {
            $('#show_name').text(storeData[store]['storeName'])
            $('#show_name_md').text(storeData[store]['storeName'])
            document.title = storeData[store]['storeName']
            var txt = `<li class="menu_active">
                        <a rel="noopener" href="./update-sales.html?store=${store}&role=staffer">
                          <i class="material-icons">assignment_late</i> บันทึกยอดขายสินค้า
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-list.html?store=${store}&role=staffer">
                          <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="other">
                        <a rel="noopener" href="./ingredient-assessment.html?store=${store}&role=staffer">
                          <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                        </a>
                      </li>
                      <li class="back_page">
                        <a role="button" href="./store-list.html">
                          <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                        </a>
                      </li>`
            $('#list_menus').append(txt)
            $('#list_menus_md').append(txt)
          })
        }
        firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
          $('#delStoreName').text(snapshot.val())
        })
      } else {
        window.location.href = './index.html'
      }
    })
}

const offlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                        <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                      </a>
                    </li>
                    <li class="other">
                      <a role="button" href="#" class="text-muted">
                        <i class="material-icons">delete</i> ลบร้านค้า
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        localforage.getItem('storesManager', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'staffer') {
        localforage.getItem('storesStaffer', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_turned_in</i> ผลการประเมินใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      }
    } else {
      window.location.href = './index.html'
    }
  })
}

window.addEventListener('online',  ()=>{
  onlineWorker()
})

window.addEventListener('offline', ()=>{
  offlineWorker()
})

function getSalePrice(key) {
  firebase.database().ref(`stores/${store}/sell/${d.getFullYear()}-${d.getMonth()}/${key}`).on(`value`, snapshot => {
    if (snapshot.val() != null) {
      $(`#${key} td:nth-child(3)`).html(snapshot.val()[`sales`])
    } else {
      $(`#${key} td:nth-child(3)`).html(`0`)
    }
  })
}

function updateSell(ele) {
  $(`#productId`).val(ele.attr(`id`))
  $(`#productPrice`).val(ele.attr(`data-price`))
  $(`#cumulativeSales`).val(ele.children(":nth-child(3)").text())
  $(`#productName`).html(ele.children(":nth-child(2)").text())
  $(`#productSales`).html(ele.children(":nth-child(3)").text())
  $(`#addSales`).modal()
}

const inputNumber = (e) => {
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
  (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
  (e.keyCode >= 35 && e.keyCode <= 39)) {
      return;
  }
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
}

$(`#confirm-addSales`).click(()=> {
  var price = $(`#productPrice`).val()
  var oldSale = parseInt($(`#cumulativeSales`).val())
  var newSale = parseInt($(`#newSales`).val())
  var updateSale = oldSale + newSale
  if (newSale != 0 && newSale != null && newSale != ``) {
    firebase.database().ref(`stores/${store}/sell/${d.getFullYear()}-${d.getMonth()}`).child($(`#productId`).val()).set({
      sales : updateSale,
      price : price
    })
  }
  document.getElementById("addSales-form").reset();
})