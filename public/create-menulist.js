var storeName
var store = new URL(window.location.href).searchParams.get('store')

$(document).ready(() => {
  if ($(document).width() >= 768) {
    $('.row-menu').css('height', $(window).height() - 56)
    $('.row-menu').css('mex-height', $(window).height() - 56)
  }
  if (navigator.onLine) {
    onlineWorker()
  } else {
    offlineWorker()
  }

  firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
    storeName = snapshot.val()
  })

  firebase.database().ref('stores/' + store + '/menuList').on('value', snapshot => {
    $(`#menu-list-table tbody`).empty()
    var i = 1
    for (var key in snapshot.val()) {
      var txt = `<tr id="${key}" onclick="actionList($(this))">
                  <td>${i}</td>
                  <td>${snapshot.val()[key][`menuName`]}</td>
                  <td>${formatMoney(parseInt(snapshot.val()[key][`menuPrice`]))}</td>
                </tr>`
      i++
      $(`#menu-list-table tbody`).append(txt)
    }
    if (snapshot.numChildren() != 0) {
      $(`#help-table`).removeClass(`d-none`)
      $(`#table-empty-alert`).addClass(`d-none`)
    } else {
      $(`#help-table`).addClass(`d-none`)
      $(`#table-empty-alert`).removeClass(`d-none`)
    }
  })
})

const actionList = (elem) => {
  $(`#delProduct-btn`).attr(`data-target`, elem.attr(`id`))
  $(`#editProduct-btn`).attr(`data-target`, elem.attr(`id`))
  $(`#editProductName`).val(elem.children(":nth-child(2)").html())
  $(`#editProductPrice`).val(elem.children(":nth-child(3)").html())
  $(`#actionProduct`).text(elem.children(":nth-child(2)").html())
  $(`#actionPrice`).text(elem.children(":nth-child(3)").html())
  $(`#editRow`).modal()
}

$('#delConfirm').keyup(() => {
  if ($('#delConfirm').val() !== storeName) {
    $('#delConfirm-btn').prop('disabled', true)
  } else {
    $('#delConfirm-btn').prop('disabled', false)
  }
})

$('#delConfirm-btn').click(() => {
  firebase.database().ref('stores/' + store).remove().then(() => {
    window.location.href = './store-list.html'
  }).catch((error) => {
    console.log(error)
  })
})

const offlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                      <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                        <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                      </a>
                    </li>
                    <li class="menu_active">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                      </a>
                    </li>
                    <li class="other">
                      <a rel="noopener" href="#" class="text-muted">
                        <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                      </a>
                    </li>
                    <li class="other">
                      <a role="button" href="#" class="text-muted">
                        <i class="material-icons">delete</i> ลบร้านค้า
                      </a>
                    </li>
                    <li class="back_page">
                      <a role="button" href="./store-list.html">
                        <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                      </a>
                    </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        window.location.href = './update-sales.html?store=' + store + '&role=manager'
      } else if (role === 'staffer') {
        window.location.href = './update-sales.html?store=' + store + '&role=staffer'
      }
    } else {
      window.location.href = './index.html'
    }
  })
}

const onlineWorker = () => {
  $('#list_menus').empty()
  $('#list_menus_md').empty()
  var url = new URL(window.location.href)
  var store = url.searchParams.get('store')
  var role = url.searchParams.get('role')
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      if (role === 'owner') {
        localforage.getItem('storesOwner', (err, storeData) => {
          $('#show_name').text(storeData[store]['storeName'])
          $('#show_name_md').text(storeData[store]['storeName'])
          document.title = storeData[store]['storeName']
          var txt = `<li class="other">
                          <a rel="noopener" href="./store-menu.html?store=${store}&role=owner">
                            <i class="material-icons">bar_chart</i> ภาพรวมผลประกอบการ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./update-sales.html?store=${store}&role=owner">
                            <i class="material-icons">attach_money</i> บันทึกยอดขายสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-request.html?store=${store}&role=owner">
                            <i class="material-icons">assignment_late</i> ใบสั่งซื้อรอการอนุมัติ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./ingredient-list.html?store=${store}&role=owner">
                            <i class="material-icons">assignment</i> สร้างใบสั่งซื้อ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./expenses.html?store=${store}&role=owner">
                            <i class="material-icons">receipt</i> บันทึกรายจ่ายอื่น ๆ
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./edit-access.html?store=${store}&role=owner">
                            <i class="material-icons">accessibility</i> แก้ไขสิทธิ์การเข้าถึงข้อมูล
                          </a>
                        </li>
                        <li class="menu_active">
                          <a rel="noopener" href="./create-menulist.html?store=${store}&role=owner">
                            <i class="material-icons">format_list_numbered</i> กำหนดรายการสินค้า
                          </a>
                        </li>
                        <li class="other">
                          <a rel="noopener" href="./edit-store.html?store=${store}&role=owner">
                            <i class="material-icons">store</i> แก้ไขข้อมูลร้าน
                          </a>
                        </li>
                        <li class="del_store">
                          <a role="button" href="#" data-toggle="modal" data-target="#delStore">
                            <i class="material-icons">delete</i> ลบร้านค้า
                          </a>
                        </li>
                        <li class="back_page">
                          <a role="button" href="./store-list.html">
                            <i class="material-icons">arrow_back</i> กลับสู่เมนูหลัก
                          </a>
                        </li>`
          $('#list_menus').append(txt)
          $('#list_menus_md').append(txt)
        })
      } else if (role === 'manager') {
        window.location.href = './update-sales.html?store=' + store + '&role=manager'
      } else if (role === 'staffer') {
        window.location.href = './update-sales.html?store=' + store + '&role=staffer'
      }
      firebase.database().ref('stores/' + store + '/storeName').on('value', snapshot => {
        $('#delStoreName').text(snapshot.val())
      })
    } else {
      window.location.href = './index.html'
    }
  })
}

window.addEventListener('online', () => {
  onlineWorker()
})

window.addEventListener('offline', () => {
  offlineWorker()
})

window.onresize = (event) => {
  $('.row-menu').css('height', $(window).height() - 56)
}

const inputNumber = (e) => {
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
    (e.keyCode >= 35 && e.keyCode <= 39)) {
    return;
  }
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
}

$(`#productName`).keyup(() => {
  if ($(`#productName`).val() !== `` && $(`#productPrice`).val() !== ``) {
    $(`#confirm-addMenu`).prop(`disabled`, false)
  }
})

$(`#productPrice`).keyup(() => {
  if ($(`#productName`).val() !== `` && $(`#productPrice`).val() !== ``) {
    $(`#confirm-addMenu`).prop(`disabled`, false)
  }
})

$(`#confirm-addMenu`).click(() => {
  firebase.database().ref(`stores/${store}/menuList`).push({
    menuName: $(`#productName`).val(),
    menuPrice: $(`#productPrice`).val()
  })
  document.getElementById("addProduct-form").reset();
})

const formatMoney = (num) => {
  if (num !== ``) {
    var p = num.toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
  } else {
    return `0.00`
  }
}

$(`#delProduct-btn`).click(function() {
  var target = $(this).attr(`data-target`)
  firebase.database().ref(`stores/${store}/menuList/${target}`).remove()
  $(`#editRow`).modal(`hide`)
})

$(`#editProduct-btn`).click(function() {
  $(`#editRow`).modal(`hide`)
  $(`#editProduct`).modal()
})

$(`#confirm-saveEdit`).click(()=>{
  var target = $(`#editProduct-btn`).attr(`data-target`)
  firebase.database().ref(`stores/${store}/menuList`).child(target).set({
    menuName: $(`#editProductName`).val(),
    menuPrice: $(`#editProductPrice`).val()
  })
  $(`#editProduct`).modal(`hide`)
})